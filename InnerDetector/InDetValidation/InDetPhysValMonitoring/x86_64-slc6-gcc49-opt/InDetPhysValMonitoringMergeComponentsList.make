#-- start of make_header -----------------

#====================================
#  Document InDetPhysValMonitoringMergeComponentsList
#
#   Generated Wed Feb  7 18:57:23 2018  by sswift
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetPhysValMonitoringMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetPhysValMonitoringMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetPhysValMonitoringMergeComponentsList

InDetPhysValMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPhysValMonitoringMergeComponentsList = $(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringMergeComponentsList.make
cmt_local_tagfile_InDetPhysValMonitoringMergeComponentsList = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPhysValMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPhysValMonitoringMergeComponentsList = $(InDetPhysValMonitoring_tag).make
cmt_local_tagfile_InDetPhysValMonitoringMergeComponentsList = $(bin)$(InDetPhysValMonitoring_tag).make

endif

include $(cmt_local_tagfile_InDetPhysValMonitoringMergeComponentsList)
#-include $(cmt_local_tagfile_InDetPhysValMonitoringMergeComponentsList)

ifdef cmt_InDetPhysValMonitoringMergeComponentsList_has_target_tag

cmt_final_setup_InDetPhysValMonitoringMergeComponentsList = $(bin)setup_InDetPhysValMonitoringMergeComponentsList.make
cmt_dependencies_in_InDetPhysValMonitoringMergeComponentsList = $(bin)dependencies_InDetPhysValMonitoringMergeComponentsList.in
#cmt_final_setup_InDetPhysValMonitoringMergeComponentsList = $(bin)InDetPhysValMonitoring_InDetPhysValMonitoringMergeComponentsListsetup.make
cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile = $(bin)InDetPhysValMonitoringMergeComponentsList.make

else

cmt_final_setup_InDetPhysValMonitoringMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_InDetPhysValMonitoringMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_InDetPhysValMonitoringMergeComponentsList = $(bin)InDetPhysValMonitoringsetup.make
cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile = $(bin)InDetPhysValMonitoringMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPhysValMonitoringsetup.make

#InDetPhysValMonitoringMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetPhysValMonitoringMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetPhysValMonitoringMergeComponentsList/
#InDetPhysValMonitoringMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: InDetPhysValMonitoringMergeComponentsList InDetPhysValMonitoringMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/InDetPhysValMonitoring.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

InDetPhysValMonitoringMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  InDetPhysValMonitoringMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

InDetPhysValMonitoringMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libInDetPhysValMonitoring_so_dependencies = ../x86_64-slc6-gcc49-opt/libInDetPhysValMonitoring.so
#-- start of cleanup_header --------------

clean :: InDetPhysValMonitoringMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetPhysValMonitoringMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetPhysValMonitoringMergeComponentsListclean ::
#-- end of cleanup_header ---------------
