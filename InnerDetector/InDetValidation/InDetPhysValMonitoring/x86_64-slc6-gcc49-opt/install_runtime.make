#-- start of make_header -----------------

#====================================
#  Document install_runtime
#
#   Generated Wed Feb  7 18:53:39 2018  by sswift
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_install_runtime_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_install_runtime_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_install_runtime

InDetPhysValMonitoring_tag = $(tag)

#cmt_local_tagfile_install_runtime = $(InDetPhysValMonitoring_tag)_install_runtime.make
cmt_local_tagfile_install_runtime = $(bin)$(InDetPhysValMonitoring_tag)_install_runtime.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPhysValMonitoring_tag = $(tag)

#cmt_local_tagfile_install_runtime = $(InDetPhysValMonitoring_tag).make
cmt_local_tagfile_install_runtime = $(bin)$(InDetPhysValMonitoring_tag).make

endif

include $(cmt_local_tagfile_install_runtime)
#-include $(cmt_local_tagfile_install_runtime)

ifdef cmt_install_runtime_has_target_tag

cmt_final_setup_install_runtime = $(bin)setup_install_runtime.make
cmt_dependencies_in_install_runtime = $(bin)dependencies_install_runtime.in
#cmt_final_setup_install_runtime = $(bin)InDetPhysValMonitoring_install_runtimesetup.make
cmt_local_install_runtime_makefile = $(bin)install_runtime.make

else

cmt_final_setup_install_runtime = $(bin)setup.make
cmt_dependencies_in_install_runtime = $(bin)dependencies.in
#cmt_final_setup_install_runtime = $(bin)InDetPhysValMonitoringsetup.make
cmt_local_install_runtime_makefile = $(bin)install_runtime.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPhysValMonitoringsetup.make

#install_runtime :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'install_runtime'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = install_runtime/
#install_runtime::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------


ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/share

install_runtime :: install_runtimeinstall ;

install :: install_runtimeinstall ;

install_runtimeclean :: install_runtimeuninstall

uninstall :: install_runtimeuninstall


# This is to avoid error in case there are no files to install
# Ideally, the fragment should not be used without files to install,
# and this line should be dropped then
install_runtimeinstall :: ;

inDetPhysValMonitoringPlotDefinitions_hdef_dependencies = ../share/inDetPhysValMonitoringPlotDefinitions.hdef
entityDaughter_xml_dependencies = ../share/entityDaughter.xml
testHDef_xml_dependencies = ../share/testHDef.xml
entityMaster_xml_dependencies = ../share/entityMaster.xml
LargeD0PlotDefinitions_xml_dependencies = ../share/LargeD0PlotDefinitions.xml
inc_xml_dependencies = ../share/inc.xml
InDetPVMPlotDefRun2_xml_dependencies = ../share/InDetPVMPlotDefRun2.xml
InDetPVMPlotDefITK_xml_dependencies = ../share/InDetPVMPlotDefITK.xml
InDetPVMPlotDefCommon_xml_dependencies = ../share/InDetPVMPlotDefCommon.xml
hdefhtml_xsl_dependencies = ../share/hdefhtml.xsl
hdeftty_xsl_dependencies = ../share/hdeftty.xsl
InDetPhysValMonitoring_TestConfiguration_xml_dependencies = ../test/InDetPhysValMonitoring_TestConfiguration.xml
inDetPhysValMonitoringPlotDefinitions_hdef_dependencies = ../share/inDetPhysValMonitoringPlotDefinitions.hdef
entityDaughter_xml_dependencies = ../share/entityDaughter.xml
testHDef_xml_dependencies = ../share/testHDef.xml
entityMaster_xml_dependencies = ../share/entityMaster.xml
LargeD0PlotDefinitions_xml_dependencies = ../share/LargeD0PlotDefinitions.xml
inc_xml_dependencies = ../share/inc.xml
InDetPVMPlotDefRun2_xml_dependencies = ../share/InDetPVMPlotDefRun2.xml
InDetPVMPlotDefITK_xml_dependencies = ../share/InDetPVMPlotDefITK.xml
InDetPVMPlotDefCommon_xml_dependencies = ../share/InDetPVMPlotDefCommon.xml
hdefhtml_xsl_dependencies = ../share/hdefhtml.xsl
hdeftty_xsl_dependencies = ../share/hdeftty.xsl
InDetPhysValMonitoring_TestConfiguration_xml_dependencies = ../test/InDetPhysValMonitoring_TestConfiguration.xml


install_runtimeinstall :: ${install_dir}/inDetPhysValMonitoringPlotDefinitions.hdef ;

${install_dir}/inDetPhysValMonitoringPlotDefinitions.hdef :: ../share/inDetPhysValMonitoringPlotDefinitions.hdef
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/inDetPhysValMonitoringPlotDefinitions.hdef`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "inDetPhysValMonitoringPlotDefinitions.hdef" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/inDetPhysValMonitoringPlotDefinitions.hdef : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/inDetPhysValMonitoringPlotDefinitions.hdef`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "inDetPhysValMonitoringPlotDefinitions.hdef" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/entityDaughter.xml ;

${install_dir}/entityDaughter.xml :: ../share/entityDaughter.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/entityDaughter.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "entityDaughter.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/entityDaughter.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/entityDaughter.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "entityDaughter.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/testHDef.xml ;

${install_dir}/testHDef.xml :: ../share/testHDef.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/testHDef.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "testHDef.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/testHDef.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/testHDef.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "testHDef.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/entityMaster.xml ;

${install_dir}/entityMaster.xml :: ../share/entityMaster.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/entityMaster.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "entityMaster.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/entityMaster.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/entityMaster.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "entityMaster.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/LargeD0PlotDefinitions.xml ;

${install_dir}/LargeD0PlotDefinitions.xml :: ../share/LargeD0PlotDefinitions.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/LargeD0PlotDefinitions.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "LargeD0PlotDefinitions.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/LargeD0PlotDefinitions.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/LargeD0PlotDefinitions.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "LargeD0PlotDefinitions.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/inc.xml ;

${install_dir}/inc.xml :: ../share/inc.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/inc.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "inc.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/inc.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/inc.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "inc.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/InDetPVMPlotDefRun2.xml ;

${install_dir}/InDetPVMPlotDefRun2.xml :: ../share/InDetPVMPlotDefRun2.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefRun2.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "InDetPVMPlotDefRun2.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/InDetPVMPlotDefRun2.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefRun2.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "InDetPVMPlotDefRun2.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/InDetPVMPlotDefITK.xml ;

${install_dir}/InDetPVMPlotDefITK.xml :: ../share/InDetPVMPlotDefITK.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefITK.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "InDetPVMPlotDefITK.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/InDetPVMPlotDefITK.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefITK.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "InDetPVMPlotDefITK.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/InDetPVMPlotDefCommon.xml ;

${install_dir}/InDetPVMPlotDefCommon.xml :: ../share/InDetPVMPlotDefCommon.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefCommon.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "InDetPVMPlotDefCommon.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/InDetPVMPlotDefCommon.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefCommon.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "InDetPVMPlotDefCommon.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/hdefhtml.xsl ;

${install_dir}/hdefhtml.xsl :: ../share/hdefhtml.xsl
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/hdefhtml.xsl`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "hdefhtml.xsl" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/hdefhtml.xsl : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/hdefhtml.xsl`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "hdefhtml.xsl" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/hdeftty.xsl ;

${install_dir}/hdeftty.xsl :: ../share/hdeftty.xsl
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/hdeftty.xsl`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "hdeftty.xsl" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/hdeftty.xsl : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/hdeftty.xsl`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "hdeftty.xsl" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/InDetPhysValMonitoring_TestConfiguration.xml ;

${install_dir}/InDetPhysValMonitoring_TestConfiguration.xml :: ../test/InDetPhysValMonitoring_TestConfiguration.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../test/InDetPhysValMonitoring_TestConfiguration.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "InDetPhysValMonitoring_TestConfiguration.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../test/InDetPhysValMonitoring_TestConfiguration.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../test/InDetPhysValMonitoring_TestConfiguration.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "InDetPhysValMonitoring_TestConfiguration.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/inDetPhysValMonitoringPlotDefinitions.hdef ;

${install_dir}/inDetPhysValMonitoringPlotDefinitions.hdef :: ../share/inDetPhysValMonitoringPlotDefinitions.hdef
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/inDetPhysValMonitoringPlotDefinitions.hdef`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "inDetPhysValMonitoringPlotDefinitions.hdef" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/inDetPhysValMonitoringPlotDefinitions.hdef : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/inDetPhysValMonitoringPlotDefinitions.hdef`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "inDetPhysValMonitoringPlotDefinitions.hdef" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/entityDaughter.xml ;

${install_dir}/entityDaughter.xml :: ../share/entityDaughter.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/entityDaughter.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "entityDaughter.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/entityDaughter.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/entityDaughter.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "entityDaughter.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/testHDef.xml ;

${install_dir}/testHDef.xml :: ../share/testHDef.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/testHDef.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "testHDef.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/testHDef.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/testHDef.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "testHDef.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/entityMaster.xml ;

${install_dir}/entityMaster.xml :: ../share/entityMaster.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/entityMaster.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "entityMaster.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/entityMaster.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/entityMaster.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "entityMaster.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/LargeD0PlotDefinitions.xml ;

${install_dir}/LargeD0PlotDefinitions.xml :: ../share/LargeD0PlotDefinitions.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/LargeD0PlotDefinitions.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "LargeD0PlotDefinitions.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/LargeD0PlotDefinitions.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/LargeD0PlotDefinitions.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "LargeD0PlotDefinitions.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/inc.xml ;

${install_dir}/inc.xml :: ../share/inc.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/inc.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "inc.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/inc.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/inc.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "inc.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/InDetPVMPlotDefRun2.xml ;

${install_dir}/InDetPVMPlotDefRun2.xml :: ../share/InDetPVMPlotDefRun2.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefRun2.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "InDetPVMPlotDefRun2.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/InDetPVMPlotDefRun2.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefRun2.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "InDetPVMPlotDefRun2.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/InDetPVMPlotDefITK.xml ;

${install_dir}/InDetPVMPlotDefITK.xml :: ../share/InDetPVMPlotDefITK.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefITK.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "InDetPVMPlotDefITK.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/InDetPVMPlotDefITK.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefITK.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "InDetPVMPlotDefITK.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/InDetPVMPlotDefCommon.xml ;

${install_dir}/InDetPVMPlotDefCommon.xml :: ../share/InDetPVMPlotDefCommon.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefCommon.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "InDetPVMPlotDefCommon.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/InDetPVMPlotDefCommon.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/InDetPVMPlotDefCommon.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "InDetPVMPlotDefCommon.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/hdefhtml.xsl ;

${install_dir}/hdefhtml.xsl :: ../share/hdefhtml.xsl
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/hdefhtml.xsl`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "hdefhtml.xsl" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/hdefhtml.xsl : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/hdefhtml.xsl`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "hdefhtml.xsl" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/hdeftty.xsl ;

${install_dir}/hdeftty.xsl :: ../share/hdeftty.xsl
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/hdeftty.xsl`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "hdeftty.xsl" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../share/hdeftty.xsl : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/hdeftty.xsl`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "hdeftty.xsl" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_runtimeinstall :: ${install_dir}/InDetPhysValMonitoring_TestConfiguration.xml ;

${install_dir}/InDetPhysValMonitoring_TestConfiguration.xml :: ../test/InDetPhysValMonitoring_TestConfiguration.xml
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../test/InDetPhysValMonitoring_TestConfiguration.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "InDetPhysValMonitoring_TestConfiguration.xml" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/AtlasCore/20.20.10/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../test/InDetPhysValMonitoring_TestConfiguration.xml : ;

install_runtimeuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../test/InDetPhysValMonitoring_TestConfiguration.xml`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "InDetPhysValMonitoring_TestConfiguration.xml" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi
#-- start of cleanup_header --------------

clean :: install_runtimeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(install_runtime.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

install_runtimeclean ::
#-- end of cleanup_header ---------------
