#-- start of make_header -----------------

#====================================
#  Document InDetPhysValMonitoringConf
#
#   Generated Wed Feb  7 18:57:20 2018  by sswift
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetPhysValMonitoringConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetPhysValMonitoringConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetPhysValMonitoringConf

InDetPhysValMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPhysValMonitoringConf = $(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringConf.make
cmt_local_tagfile_InDetPhysValMonitoringConf = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPhysValMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPhysValMonitoringConf = $(InDetPhysValMonitoring_tag).make
cmt_local_tagfile_InDetPhysValMonitoringConf = $(bin)$(InDetPhysValMonitoring_tag).make

endif

include $(cmt_local_tagfile_InDetPhysValMonitoringConf)
#-include $(cmt_local_tagfile_InDetPhysValMonitoringConf)

ifdef cmt_InDetPhysValMonitoringConf_has_target_tag

cmt_final_setup_InDetPhysValMonitoringConf = $(bin)setup_InDetPhysValMonitoringConf.make
cmt_dependencies_in_InDetPhysValMonitoringConf = $(bin)dependencies_InDetPhysValMonitoringConf.in
#cmt_final_setup_InDetPhysValMonitoringConf = $(bin)InDetPhysValMonitoring_InDetPhysValMonitoringConfsetup.make
cmt_local_InDetPhysValMonitoringConf_makefile = $(bin)InDetPhysValMonitoringConf.make

else

cmt_final_setup_InDetPhysValMonitoringConf = $(bin)setup.make
cmt_dependencies_in_InDetPhysValMonitoringConf = $(bin)dependencies.in
#cmt_final_setup_InDetPhysValMonitoringConf = $(bin)InDetPhysValMonitoringsetup.make
cmt_local_InDetPhysValMonitoringConf_makefile = $(bin)InDetPhysValMonitoringConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPhysValMonitoringsetup.make

#InDetPhysValMonitoringConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetPhysValMonitoringConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetPhysValMonitoringConf/
#InDetPhysValMonitoringConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: InDetPhysValMonitoringConf InDetPhysValMonitoringConfclean

confpy  := InDetPhysValMonitoringConf.py
conflib := $(bin)$(library_prefix)InDetPhysValMonitoring.$(shlibsuffix)
confdb  := InDetPhysValMonitoring.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

InDetPhysValMonitoringConf :: InDetPhysValMonitoringConfinstall

install :: InDetPhysValMonitoringConfinstall

InDetPhysValMonitoringConfinstall : /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring/$(confpy)
	@echo "Installing /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring in /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InstallArea/python ; \

/afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring/$(confpy) : $(conflib) /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring
	$(genconf_silent) $(genconfig_cmd)   -o /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)InDetPhysValMonitoring.$(shlibsuffix)

/afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring:
	@ if [ ! -d /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring ] ; then mkdir -p /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring ; fi ;

InDetPhysValMonitoringConfclean :: InDetPhysValMonitoringConfuninstall
	$(cleanup_silent) $(remove_command) /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring/$(confpy) /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation/InDetPhysValMonitoring/genConf/InDetPhysValMonitoring/$(confdb)

uninstall :: InDetPhysValMonitoringConfuninstall

InDetPhysValMonitoringConfuninstall ::
	@$(uninstall_command) /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InstallArea/python
libInDetPhysValMonitoring_so_dependencies = ../x86_64-slc6-gcc49-opt/libInDetPhysValMonitoring.so
#-- start of cleanup_header --------------

clean :: InDetPhysValMonitoringConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetPhysValMonitoringConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetPhysValMonitoringConfclean ::
#-- end of cleanup_header ---------------
