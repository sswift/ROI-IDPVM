# echo "setup InDetPhysValMonitoring InDetPhysValMonitoring-r811155 in /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/CMT/v1r25p20160527
endif
source ${CMTROOT}/mgr/setup.csh
set cmtInDetPhysValMonitoringtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtInDetPhysValMonitoringtempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=InDetPhysValMonitoring -version=InDetPhysValMonitoring-r811155 -path=/afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation  -quiet -without_version_directory -no_cleanup $* >${cmtInDetPhysValMonitoringtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=InDetPhysValMonitoring -version=InDetPhysValMonitoring-r811155 -path=/afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation  -quiet -without_version_directory -no_cleanup $* >${cmtInDetPhysValMonitoringtempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtInDetPhysValMonitoringtempfile}
  unset cmtInDetPhysValMonitoringtempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtInDetPhysValMonitoringtempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtInDetPhysValMonitoringtempfile}
unset cmtInDetPhysValMonitoringtempfile
exit $cmtsetupstatus

