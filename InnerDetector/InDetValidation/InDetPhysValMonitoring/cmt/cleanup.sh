# echo "cleanup InDetPhysValMonitoring InDetPhysValMonitoring-r811155 in /afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.10/CMT/v1r25p20160527; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtInDetPhysValMonitoringtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtInDetPhysValMonitoringtempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=InDetPhysValMonitoring -version=InDetPhysValMonitoring-r811155 -path=/afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation  -quiet -without_version_directory $* >${cmtInDetPhysValMonitoringtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=InDetPhysValMonitoring -version=InDetPhysValMonitoring-r811155 -path=/afs/cern.ch/user/s/sswift/InDetPhysValMonitoring/trunk-nov3/InnerDetector/InDetValidation  -quiet -without_version_directory $* >${cmtInDetPhysValMonitoringtempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtInDetPhysValMonitoringtempfile}
  unset cmtInDetPhysValMonitoringtempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtInDetPhysValMonitoringtempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtInDetPhysValMonitoringtempfile}
unset cmtInDetPhysValMonitoringtempfile
return $cmtcleanupstatus

