#ifndef INDETPHYSVALMONITORING_INDETPERFPLOT_SPECTRUM
#define INDETPHYSVALMONITORING_INDETPERFPLOT_SPECTRUM
/**
 * @file InDetPerfPlot_spectrum.h
 * @author Stewart Swift
 **/


// std includes
#include <string>

// local includes
#include "InDetPlotBase.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/Vertex.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/EgammaxAODHelpers.h"


class InDetPerfPlot_spectrum: public InDetPlotBase {
public:
  enum Category {
    ALL, IN_JETS, N_FAKETYPES
  };
  InDetPerfPlot_spectrum(InDetPlotBase* pParent, const std::string& dirName);
  void fillSpectrum(const xAOD::TrackParticle& trkprt, Float_t prob);
  void fillSpectrum(const xAOD::TruthParticle& particle);
  void fillSpectrum(const xAOD::TrackParticle& trkprt, const xAOD::TruthVertex& truthVrt);
  void fillSpectrum(const xAOD::TrackParticle& trkprt, const xAOD::Vertex& vertex, bool fill);
  void fillSpectrumUnlinked(const xAOD::TrackParticle& trkprt, Float_t prob);
  void fillSpectrumLinked(const xAOD::TrackParticle& trkprt, Float_t prob);
  void fillSpectrumLinked(const xAOD::TrackParticle& trkprt, const xAOD::TruthParticle& particle, double prob);
  void fillSpectrumUnlinked2(const xAOD::TrackParticle& trkprt, double prob);
  void fillPhoton(const xAOD::TruthParticle& photon, bool weight_efficiency, int mu) ;
  void fillPhoton(const xAOD::TruthParticle& photon,int convweight);
  void fillPhotonResolution(const xAOD::TruthParticle& photon,const xAOD::Photon* rphoton, int convType);
  void fillPhotonResolution(const xAOD::TruthParticle& photon,const xAOD::Photon* rphoton);
private:
  TH1* m_nSCTHits;
  TH1* m_nPixHits;
  TH1* m_nTotHits;
  TH1* m_nSCTDeadSensors;
  TH1* m_nPixDeadSensors;
  TH1* m_nTotDeadSensors;

  TH1* m_recoMatchProb;
  TH1* m_recoPt;
  TH1* m_recoEta;
  TH1* m_recoPhi;
  TH1* m_recod0;
  TH1* m_recoz0;
  TH1* m_recoz0sin;
  //
  TH1* m_recod0_TruthVtxR;
  TH1* m_recoz0_TruthVtxZ;
  TH1* m_recoz0_TruthVtxZsin;
  TH1* m_TruthVtxR;
  TH1* m_TruthVtxZ;
  TH1* m_TruthVtxX;
  TH1* m_TruthVtxY;

  TH2* m_TVR_vs_Z;
  TH2* m_recod0_vs_z0_good;
  TH2* m_recod0_vs_z0_crazy;

  TH1* m_truthPt;
  TH1* m_truthEta;
  TH1* m_truthPhi;

  // PV plots
  TH1* m_recod0_PrimVtxR;
  TH1* m_recoz0_PrimVtxZ;
  TH1* m_recoz0_PrimVtxZsin;
  TH1* m_PrimVtxR;
  TH1* m_PrimVtxZ;
  TH1* m_PrimVtxX;
  TH1* m_PrimVtxY;

  TH2* m_PVR_vs_Z;

  TH2* m_nSCTHits_vs_eta;
  TH2* m_nPixHits_vs_eta;
  TH2* m_nTotHits_vs_eta;
  TH2* m_nSCTHoles_vs_eta;
  TH2* m_nPixHoles_vs_eta;
  TH2* m_nTotHoles_vs_eta;
  TH2* m_nSCTOutliers_vs_eta;
  TH2* m_nPixOutliers_vs_eta;
  TH2* m_nTotOutliers_vs_eta;
  TH2F* m_nSCTHits_phi_vs_eta;
  TH2F* m_nPixHits_phi_vs_eta;
  TH2F* m_nTotHits_phi_vs_eta;

  TProfile* m_nSCTDeadSensors_vs_eta;
  TProfile* m_nPixDeadSensors_vs_eta;
  TProfile* m_nTotDeadSensors_vs_eta;
  TH2* m_ptvsEtaUNLINKED;
  TH2* m_ptvsEtaLINKED;

  TH2* m_recoMatchvsSiHitsUNLINKED;
  TH2* m_recoMatchvsSCTHitsUNLINKED;
  TH2* m_recoMatchvsPixHitsUNLINKED;
  TH2* m_recoMatchvsSiHitsLINKED;
  TH2* m_recoMatchvsSCTHitsLINKED;
  TH2* m_recoMatchvsPixHitsLINKED;
  TProfile* m_nSCTHits_vs_etaUNLINKED;
  TProfile* m_nPixHits_vs_etaUNLINKED;
  TProfile* m_nTotHits_vs_etaUNLINKED;
  TProfile* m_nSCTHits_vs_etaLINKED;
  TProfile* m_nPixHits_vs_etaLINKED;
  TProfile* m_nTotHits_vs_etaLINKED;
  TH1* m_recoMatchProbUNLINKED;
  TH1* m_recoMatchProbLINKED;


  TH2* m_ptvsEtaUnlinked;
  TH2* m_probvsSCTUnlinked;
  TH2* m_probvsPixUnlinked;
  TH2* m_sharedHitsvsSCTUnlinked;
  TH2* m_sharedHitsvsPixUnlinked;
  TH2* m_pixholesvsPixUnlinked;
  TH2* m_holesvsPixUnlinked;
  TH2* m_sctholesvsSCTUnlinked;
  TH2* m_holesvsSCTUnlinked;
  TH2* m_outliersvsPixUnlinked;
  TH2* m_pixoutliersvsPixUnlinked;
  TH2* m_outliersvsSCTUnlinked;
  TH2* m_sctoutliersvsSCTUnlinked;
  TProfile* m_hitsvsEtaUnlinked;
  TProfile* m_pixHolesvsEtaUnlinked;
  TProfile* m_sctHolesvsEtaUnlinked;
  TH2* m_sctHitsvsPixHitsUnlinked;
  TProfile* m_sctHitsvsEtaUnlinked;
  TProfile* m_pixHitsvsEtaUnlinked;
  TProfile* m_pixOutliersvsEtaUnlinked;
  TProfile* m_sctOutliersvsEtaUnlinked;

  TH2* m_ptvsEtaLinked;
  TH2* m_probvsSCTLinked;
  TH2* m_probvsPixLinked;
  TH2* m_sharedHitsvsSCTLinked;
  TH2* m_sharedHitsvsPixLinked;
  TH2* m_pixholesvsPixLinked;
  TH2* m_holesvsPixLinked;
  TH2* m_sctholesvsSCTLinked;
  TH2* m_holesvsSCTLinked;
  TH2* m_outliersvsPixLinked;
  TH2* m_pixoutliersvsPixLinked;
  TH2* m_sctoutliersvsSCTLinked;
  TH2* m_outliersvsSCTLinked;
  TProfile* m_hitsvsEtaLinked;
  TProfile* m_pixHolesvsEtaLinked;
  TProfile* m_sctHolesvsEtaLinked;
  TH2* m_sctHitsvsPixHitsLinked;
  TProfile* m_sctHitsvsEtaLinked;
  TProfile* m_pixHitsvsEtaLinked;
  TProfile* m_pixOutliersvsEtaLinked;
  TProfile* m_sctOutliersvsEtaLinked;

  TEfficiency* m_photoneff_R_etabin0;
  TEfficiency* m_photoneff_R_etabin1;
  TEfficiency* m_photoneff_R_etabin2;
  TEfficiency* m_photoneff_R_etabin3;
  TEfficiency* m_photoneff_R_etabin4;
  TEfficiency* m_photoneff_mu_etabin0;
  TEfficiency* m_photoneff_mu_etabin1;
  TEfficiency* m_photoneff_mu_etabin2;
  TEfficiency* m_photoneff_mu_etabin3;
  TEfficiency* m_photoneff_mu_etabin4;
  TEfficiency* m_photoneff_eta;
  TEfficiency* m_photonconvfraction_eta;
  TH1* m_photoneff_R_numerator;
  TH1* m_photoneff_R_denominator;
  TH1* m_photoneff_Eta_numerator;
  TH1* m_photoneff_Eta_denominator;
  TH2* m_photoneff_RvsZ_numerator;
  TH2* m_photoneff_RvsZ_denominator;
  TH2* m_photoneff_RvsEta_numerator;
  TH2* m_photoneff_RvsEta_denominator;
  TH2* m_photoneff_XvsY_numerator;
  TH2* m_photoneff_XvsY_denominator;

  TEfficiency* m_pt15300_eta01_R;
  TEfficiency* m_pt15300_eta12_R;
  TEfficiency* m_pt15300_eta23_R;
  TEfficiency* m_pt15300_eta34_R;
  TEfficiency* m_pt300600_eta01_R;
  TEfficiency* m_pt300600_eta12_R;
  TEfficiency* m_pt300600_eta23_R;
  TEfficiency* m_pt300600_eta34_R;
  TEfficiency* m_pt600_eta01_R;
  TEfficiency* m_pt600_eta12_R;
  TEfficiency* m_pt600_eta23_R;
  TEfficiency* m_pt600_eta34_R;
  TH2* m_photoneff_PtvsEta_numerator;
  TH2* m_photoneff_PtvsEta_denominator;

  TH2* m_photonres_EtavsEta;
  TH2* m_photonres_PhivsEta;
  TH2* m_photonres_PtvsEta;
  TH2* m_photonres_EtavsPt;
  TH2* m_photonres_PhivsPt;
  TH2* m_photonres_PtvsPt;


  TH2* m_photonres_EtavsEta_2track;
  TH2* m_photonres_PhivsEta_2track;
  TH2* m_photonres_PtvsEta_2track;
  TH2* m_photonres_EtavsPt_2track;
  TH2* m_photonres_PhivsPt_2track;
  TH2* m_photonres_PtvsPt_2track;
  TH2* m_photonres_PtvsE_2track;
  TH2* m_photonres_dRvsPt_2track;
  TH2* m_photonres_dRvsE_2track;
  TH2* m_photonres_EtavsEta_Unconv;
  TH2* m_photonres_PhivsEta_Unconv;
  TH2* m_photonres_PtvsEta_Unconv;
  TH2* m_photonres_EtavsPt_Unconv;
  TH2* m_photonres_PhivsPt_Unconv;
  TH2* m_photonres_PtvsPt_Unconv;
  TH2* m_photonres_PtvsE_Unconv;
  TH2* m_photonres_dRvsPt_Unconv;
  TH2* m_photonres_dRvsE_Unconv;
  TH2* m_photonres_EtavsEta_1track;
  TH2* m_photonres_PhivsEta_1track;
  TH2* m_photonres_PtvsEta_1track;
  TH2* m_photonres_EtavsPt_1track;
  TH2* m_photonres_PhivsPt_1track;
  TH2* m_photonres_PtvsPt_1track;
  TH2* m_photonres_PtvsE_1track;
  TH2* m_photonres_dRvsPt_1track;
  TH2* m_photonres_dRvsE_1track;
  TH2* m_photonres_RvsPt_1track;
  TH2* m_photonres_RvsEta_1track;
  TH2* m_photonres_RvsR_1track;
  TH2* m_photonres_RvsPt_2track;
  TH2* m_photonres_RvsEta_2track;
  TH2* m_photonres_RvsR_2track;
  // plot base has nop default implementation of this; we use it to book the histos
  void initializePlots();
};




#endif
