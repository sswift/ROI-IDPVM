#-- start of make_header -----------------

#====================================
#  Document InDetPhysValMonitoringComponentsList
#
#   Generated Wed Feb  7 18:57:22 2018  by sswift
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetPhysValMonitoringComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetPhysValMonitoringComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetPhysValMonitoringComponentsList

InDetPhysValMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPhysValMonitoringComponentsList = $(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringComponentsList.make
cmt_local_tagfile_InDetPhysValMonitoringComponentsList = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPhysValMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPhysValMonitoringComponentsList = $(InDetPhysValMonitoring_tag).make
cmt_local_tagfile_InDetPhysValMonitoringComponentsList = $(bin)$(InDetPhysValMonitoring_tag).make

endif

include $(cmt_local_tagfile_InDetPhysValMonitoringComponentsList)
#-include $(cmt_local_tagfile_InDetPhysValMonitoringComponentsList)

ifdef cmt_InDetPhysValMonitoringComponentsList_has_target_tag

cmt_final_setup_InDetPhysValMonitoringComponentsList = $(bin)setup_InDetPhysValMonitoringComponentsList.make
cmt_dependencies_in_InDetPhysValMonitoringComponentsList = $(bin)dependencies_InDetPhysValMonitoringComponentsList.in
#cmt_final_setup_InDetPhysValMonitoringComponentsList = $(bin)InDetPhysValMonitoring_InDetPhysValMonitoringComponentsListsetup.make
cmt_local_InDetPhysValMonitoringComponentsList_makefile = $(bin)InDetPhysValMonitoringComponentsList.make

else

cmt_final_setup_InDetPhysValMonitoringComponentsList = $(bin)setup.make
cmt_dependencies_in_InDetPhysValMonitoringComponentsList = $(bin)dependencies.in
#cmt_final_setup_InDetPhysValMonitoringComponentsList = $(bin)InDetPhysValMonitoringsetup.make
cmt_local_InDetPhysValMonitoringComponentsList_makefile = $(bin)InDetPhysValMonitoringComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPhysValMonitoringsetup.make

#InDetPhysValMonitoringComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetPhysValMonitoringComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetPhysValMonitoringComponentsList/
#InDetPhysValMonitoringComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = InDetPhysValMonitoring.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libInDetPhysValMonitoring.$(shlibsuffix)

InDetPhysValMonitoringComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: InDetPhysValMonitoringComponentsListinstall
InDetPhysValMonitoringComponentsListinstall :: InDetPhysValMonitoringComponentsList

uninstall :: InDetPhysValMonitoringComponentsListuninstall
InDetPhysValMonitoringComponentsListuninstall :: InDetPhysValMonitoringComponentsListclean

InDetPhysValMonitoringComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: InDetPhysValMonitoringComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetPhysValMonitoringComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetPhysValMonitoringComponentsListclean ::
#-- end of cleanup_header ---------------
