
#-- start of constituents_header ------

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

tags      = $(tag),$(CMTEXTRATAGS)

InDetPhysValMonitoring_tag = $(tag)

#cmt_local_tagfile = $(InDetPhysValMonitoring_tag).make
cmt_local_tagfile = $(bin)$(InDetPhysValMonitoring_tag).make

#-include $(cmt_local_tagfile)
include $(cmt_local_tagfile)

#cmt_local_setup = $(bin)setup$$$$.make
#cmt_local_setup = $(bin)$(package)setup$$$$.make
#cmt_final_setup = $(bin)InDetPhysValMonitoringsetup.make
cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)$(package)setup.make

cmt_build_library_linksstamp = $(bin)cmt_build_library_links.stamp
#--------------------------------------------------------

#cmt_lock_setup = /tmp/lock$(cmt_lock_pid).make
#cmt_temp_tag = /tmp/tag$(cmt_lock_pid).make

#first :: $(cmt_local_tagfile)
#	@echo $(cmt_local_tagfile) ok
#ifndef QUICK
#first :: $(cmt_final_setup) ;
#else
#first :: ;
#endif

##	@bin=`$(cmtexe) show macro_value bin`

#$(cmt_local_tagfile) : $(cmt_lock_setup)
#	@echo "#CMT> Error: $@: No such file" >&2; exit 1
#$(cmt_local_tagfile) :
#	@echo "#CMT> Warning: $@: No such file" >&2; exit
#	@echo "#CMT> Info: $@: No need to rebuild file" >&2; exit

#$(cmt_final_setup) : $(cmt_local_tagfile) 
#	$(echo) "(constituents.make) Rebuilding $@"
#	@if test ! -d $(@D); then $(mkdir) -p $(@D); fi; \
#	  if test -f $(cmt_local_setup); then /bin/rm -f $(cmt_local_setup); fi; \
#	  trap '/bin/rm -f $(cmt_local_setup)' 0 1 2 15; \
#	  $(cmtexe) -tag=$(tags) show setup >>$(cmt_local_setup); \
#	  if test ! -f $@; then \
#	    mv $(cmt_local_setup) $@; \
#	  else \
#	    if /usr/bin/diff $(cmt_local_setup) $@ >/dev/null ; then \
#	      : ; \
#	    else \
#	      mv $(cmt_local_setup) $@; \
#	    fi; \
#	  fi

#	@/bin/echo $@ ok   

#config :: checkuses
#	@exit 0
#checkuses : ;

env.make ::
	printenv >env.make.tmp; $(cmtexe) check files env.make.tmp env.make

ifndef QUICK
all :: build_library_links ;
else
all :: $(cmt_build_library_linksstamp) ;
endif

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

dirs :: requirements
	@if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi
#	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
#	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

#requirements :
#	@if test ! -r requirements ; then echo "No requirements file" ; fi

build_library_links : dirs
	$(echo) "(constituents.make) Rebuilding library links"; \
	 $(build_library_links)
#	if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi; \
#	$(build_library_links)

$(cmt_build_library_linksstamp) : $(cmt_final_setup) $(cmt_local_tagfile) $(bin)library_links.in
	$(echo) "(constituents.make) Rebuilding library links"; \
	 $(build_library_links) -f=$(bin)library_links.in -without_cmt
	$(silent) \touch $@

ifndef PEDANTIC
.DEFAULT ::
#.DEFAULT :
	$(echo) "(constituents.make) $@: No rule for such target" >&2
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of constituents_header ------
#-- start of group ------

all_groups :: all

all :: $(all_dependencies)  $(all_pre_constituents) $(all_constituents)  $(all_post_constituents)
	$(echo) "all ok."

#	@/bin/echo " all ok."

clean :: allclean

allclean ::  $(all_constituentsclean)
	$(echo) $(all_constituentsclean)
	$(echo) "allclean ok."

#	@echo $(all_constituentsclean)
#	@/bin/echo " allclean ok."

#-- end of group ------
#-- start of group ------

all_groups :: cmt_actions

cmt_actions :: $(cmt_actions_dependencies)  $(cmt_actions_pre_constituents) $(cmt_actions_constituents)  $(cmt_actions_post_constituents)
	$(echo) "cmt_actions ok."

#	@/bin/echo " cmt_actions ok."

clean :: allclean

cmt_actionsclean ::  $(cmt_actions_constituentsclean)
	$(echo) $(cmt_actions_constituentsclean)
	$(echo) "cmt_actionsclean ok."

#	@echo $(cmt_actions_constituentsclean)
#	@/bin/echo " cmt_actionsclean ok."

#-- end of group ------
#-- start of group ------

all_groups :: rulechecker

rulechecker :: $(rulechecker_dependencies)  $(rulechecker_pre_constituents) $(rulechecker_constituents)  $(rulechecker_post_constituents)
	$(echo) "rulechecker ok."

#	@/bin/echo " rulechecker ok."

clean :: allclean

rulecheckerclean ::  $(rulechecker_constituentsclean)
	$(echo) $(rulechecker_constituentsclean)
	$(echo) "rulecheckerclean ok."

#	@echo $(rulechecker_constituentsclean)
#	@/bin/echo " rulecheckerclean ok."

#-- end of group ------
#-- start of group ------

all_groups :: ctest

ctest :: $(ctest_dependencies)  $(ctest_pre_constituents) $(ctest_constituents)  $(ctest_post_constituents)
	$(echo) "ctest ok."

#	@/bin/echo " ctest ok."

clean :: allclean

ctestclean ::  $(ctest_constituentsclean)
	$(echo) $(ctest_constituentsclean)
	$(echo) "ctestclean ok."

#	@echo $(ctest_constituentsclean)
#	@/bin/echo " ctestclean ok."

#-- end of group ------
#-- start of constituent ------

cmt_install_runtime_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_install_runtime_has_target_tag

cmt_local_tagfile_install_runtime = $(bin)$(InDetPhysValMonitoring_tag)_install_runtime.make
cmt_final_setup_install_runtime = $(bin)setup_install_runtime.make
cmt_local_install_runtime_makefile = $(bin)install_runtime.make

install_runtime_extratags = -tag_add=target_install_runtime

else

cmt_local_tagfile_install_runtime = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_install_runtime = $(bin)setup.make
cmt_local_install_runtime_makefile = $(bin)install_runtime.make

endif

not_install_runtime_dependencies = { n=0; for p in $?; do m=0; for d in $(install_runtime_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
install_runtimedirs :
	@if test ! -d $(bin)install_runtime; then $(mkdir) -p $(bin)install_runtime; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)install_runtime
else
install_runtimedirs : ;
endif

ifdef cmt_install_runtime_has_target_tag

ifndef QUICK
$(cmt_local_install_runtime_makefile) : $(install_runtime_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_runtime.make"; \
	  $(cmtexe) -tag=$(tags) $(install_runtime_extratags) build constituent_config -out=$(cmt_local_install_runtime_makefile) install_runtime
else
$(cmt_local_install_runtime_makefile) : $(install_runtime_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_runtime) ] || \
	  [ ! -f $(cmt_final_setup_install_runtime) ] || \
	  $(not_install_runtime_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_runtime.make"; \
	  $(cmtexe) -tag=$(tags) $(install_runtime_extratags) build constituent_config -out=$(cmt_local_install_runtime_makefile) install_runtime; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_install_runtime_makefile) : $(install_runtime_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_runtime.make"; \
	  $(cmtexe) -f=$(bin)install_runtime.in -tag=$(tags) $(install_runtime_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_runtime_makefile) install_runtime
else
$(cmt_local_install_runtime_makefile) : $(install_runtime_dependencies) $(cmt_build_library_linksstamp) $(bin)install_runtime.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_runtime) ] || \
	  [ ! -f $(cmt_final_setup_install_runtime) ] || \
	  $(not_install_runtime_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_runtime.make"; \
	  $(cmtexe) -f=$(bin)install_runtime.in -tag=$(tags) $(install_runtime_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_runtime_makefile) install_runtime; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(install_runtime_extratags) build constituent_makefile -out=$(cmt_local_install_runtime_makefile) install_runtime

install_runtime :: $(install_runtime_dependencies) $(cmt_local_install_runtime_makefile) dirs install_runtimedirs
	$(echo) "(constituents.make) Starting install_runtime"
	@if test -f $(cmt_local_install_runtime_makefile); then \
	  $(MAKE) -f $(cmt_local_install_runtime_makefile) install_runtime; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_runtime_makefile) install_runtime
	$(echo) "(constituents.make) install_runtime done"

clean :: install_runtimeclean ;

install_runtimeclean :: $(install_runtimeclean_dependencies) ##$(cmt_local_install_runtime_makefile)
	$(echo) "(constituents.make) Starting install_runtimeclean"
	@-if test -f $(cmt_local_install_runtime_makefile); then \
	  $(MAKE) -f $(cmt_local_install_runtime_makefile) install_runtimeclean; \
	fi
	$(echo) "(constituents.make) install_runtimeclean done"
#	@-$(MAKE) -f $(cmt_local_install_runtime_makefile) install_runtimeclean

##	  /bin/rm -f $(cmt_local_install_runtime_makefile) $(bin)install_runtime_dependencies.make

install :: install_runtimeinstall ;

install_runtimeinstall :: $(install_runtime_dependencies) $(cmt_local_install_runtime_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_install_runtime_makefile); then \
	  $(MAKE) -f $(cmt_local_install_runtime_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_install_runtime_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : install_runtimeuninstall

$(foreach d,$(install_runtime_dependencies),$(eval $(d)uninstall_dependencies += install_runtimeuninstall))

install_runtimeuninstall : $(install_runtimeuninstall_dependencies) ##$(cmt_local_install_runtime_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_install_runtime_makefile); then \
	  $(MAKE) -f $(cmt_local_install_runtime_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_runtime_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: install_runtimeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ install_runtime"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ install_runtime done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_install_joboptions_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_install_joboptions_has_target_tag

cmt_local_tagfile_install_joboptions = $(bin)$(InDetPhysValMonitoring_tag)_install_joboptions.make
cmt_final_setup_install_joboptions = $(bin)setup_install_joboptions.make
cmt_local_install_joboptions_makefile = $(bin)install_joboptions.make

install_joboptions_extratags = -tag_add=target_install_joboptions

else

cmt_local_tagfile_install_joboptions = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_install_joboptions = $(bin)setup.make
cmt_local_install_joboptions_makefile = $(bin)install_joboptions.make

endif

not_install_joboptions_dependencies = { n=0; for p in $?; do m=0; for d in $(install_joboptions_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
install_joboptionsdirs :
	@if test ! -d $(bin)install_joboptions; then $(mkdir) -p $(bin)install_joboptions; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)install_joboptions
else
install_joboptionsdirs : ;
endif

ifdef cmt_install_joboptions_has_target_tag

ifndef QUICK
$(cmt_local_install_joboptions_makefile) : $(install_joboptions_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_joboptions.make"; \
	  $(cmtexe) -tag=$(tags) $(install_joboptions_extratags) build constituent_config -out=$(cmt_local_install_joboptions_makefile) install_joboptions
else
$(cmt_local_install_joboptions_makefile) : $(install_joboptions_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_joboptions) ] || \
	  [ ! -f $(cmt_final_setup_install_joboptions) ] || \
	  $(not_install_joboptions_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_joboptions.make"; \
	  $(cmtexe) -tag=$(tags) $(install_joboptions_extratags) build constituent_config -out=$(cmt_local_install_joboptions_makefile) install_joboptions; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_install_joboptions_makefile) : $(install_joboptions_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_joboptions.make"; \
	  $(cmtexe) -f=$(bin)install_joboptions.in -tag=$(tags) $(install_joboptions_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_joboptions_makefile) install_joboptions
else
$(cmt_local_install_joboptions_makefile) : $(install_joboptions_dependencies) $(cmt_build_library_linksstamp) $(bin)install_joboptions.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_joboptions) ] || \
	  [ ! -f $(cmt_final_setup_install_joboptions) ] || \
	  $(not_install_joboptions_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_joboptions.make"; \
	  $(cmtexe) -f=$(bin)install_joboptions.in -tag=$(tags) $(install_joboptions_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_joboptions_makefile) install_joboptions; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(install_joboptions_extratags) build constituent_makefile -out=$(cmt_local_install_joboptions_makefile) install_joboptions

install_joboptions :: $(install_joboptions_dependencies) $(cmt_local_install_joboptions_makefile) dirs install_joboptionsdirs
	$(echo) "(constituents.make) Starting install_joboptions"
	@if test -f $(cmt_local_install_joboptions_makefile); then \
	  $(MAKE) -f $(cmt_local_install_joboptions_makefile) install_joboptions; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_joboptions_makefile) install_joboptions
	$(echo) "(constituents.make) install_joboptions done"

clean :: install_joboptionsclean ;

install_joboptionsclean :: $(install_joboptionsclean_dependencies) ##$(cmt_local_install_joboptions_makefile)
	$(echo) "(constituents.make) Starting install_joboptionsclean"
	@-if test -f $(cmt_local_install_joboptions_makefile); then \
	  $(MAKE) -f $(cmt_local_install_joboptions_makefile) install_joboptionsclean; \
	fi
	$(echo) "(constituents.make) install_joboptionsclean done"
#	@-$(MAKE) -f $(cmt_local_install_joboptions_makefile) install_joboptionsclean

##	  /bin/rm -f $(cmt_local_install_joboptions_makefile) $(bin)install_joboptions_dependencies.make

install :: install_joboptionsinstall ;

install_joboptionsinstall :: $(install_joboptions_dependencies) $(cmt_local_install_joboptions_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_install_joboptions_makefile); then \
	  $(MAKE) -f $(cmt_local_install_joboptions_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_install_joboptions_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : install_joboptionsuninstall

$(foreach d,$(install_joboptions_dependencies),$(eval $(d)uninstall_dependencies += install_joboptionsuninstall))

install_joboptionsuninstall : $(install_joboptionsuninstall_dependencies) ##$(cmt_local_install_joboptions_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_install_joboptions_makefile); then \
	  $(MAKE) -f $(cmt_local_install_joboptions_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_joboptions_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: install_joboptionsuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ install_joboptions"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ install_joboptions done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_InDetPhysValMonitoring_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_InDetPhysValMonitoring_has_target_tag

cmt_local_tagfile_InDetPhysValMonitoring = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoring.make
cmt_final_setup_InDetPhysValMonitoring = $(bin)setup_InDetPhysValMonitoring.make
cmt_local_InDetPhysValMonitoring_makefile = $(bin)InDetPhysValMonitoring.make

InDetPhysValMonitoring_extratags = -tag_add=target_InDetPhysValMonitoring

else

cmt_local_tagfile_InDetPhysValMonitoring = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_InDetPhysValMonitoring = $(bin)setup.make
cmt_local_InDetPhysValMonitoring_makefile = $(bin)InDetPhysValMonitoring.make

endif

not_InDetPhysValMonitoring_dependencies = { n=0; for p in $?; do m=0; for d in $(InDetPhysValMonitoring_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
InDetPhysValMonitoringdirs :
	@if test ! -d $(bin)InDetPhysValMonitoring; then $(mkdir) -p $(bin)InDetPhysValMonitoring; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)InDetPhysValMonitoring
else
InDetPhysValMonitoringdirs : ;
endif

ifdef cmt_InDetPhysValMonitoring_has_target_tag

ifndef QUICK
$(cmt_local_InDetPhysValMonitoring_makefile) : $(InDetPhysValMonitoring_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoring.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoring_makefile) InDetPhysValMonitoring
else
$(cmt_local_InDetPhysValMonitoring_makefile) : $(InDetPhysValMonitoring_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoring) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoring) ] || \
	  $(not_InDetPhysValMonitoring_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoring.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoring_makefile) InDetPhysValMonitoring; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_InDetPhysValMonitoring_makefile) : $(InDetPhysValMonitoring_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoring.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoring.in -tag=$(tags) $(InDetPhysValMonitoring_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoring_makefile) InDetPhysValMonitoring
else
$(cmt_local_InDetPhysValMonitoring_makefile) : $(InDetPhysValMonitoring_dependencies) $(cmt_build_library_linksstamp) $(bin)InDetPhysValMonitoring.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoring) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoring) ] || \
	  $(not_InDetPhysValMonitoring_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoring.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoring.in -tag=$(tags) $(InDetPhysValMonitoring_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoring_makefile) InDetPhysValMonitoring; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_extratags) build constituent_makefile -out=$(cmt_local_InDetPhysValMonitoring_makefile) InDetPhysValMonitoring

InDetPhysValMonitoring :: $(InDetPhysValMonitoring_dependencies) $(cmt_local_InDetPhysValMonitoring_makefile) dirs InDetPhysValMonitoringdirs
	$(echo) "(constituents.make) Starting InDetPhysValMonitoring"
	@if test -f $(cmt_local_InDetPhysValMonitoring_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_makefile) InDetPhysValMonitoring; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_makefile) InDetPhysValMonitoring
	$(echo) "(constituents.make) InDetPhysValMonitoring done"

clean :: InDetPhysValMonitoringclean ;

InDetPhysValMonitoringclean :: $(InDetPhysValMonitoringclean_dependencies) ##$(cmt_local_InDetPhysValMonitoring_makefile)
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringclean"
	@-if test -f $(cmt_local_InDetPhysValMonitoring_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_makefile) InDetPhysValMonitoringclean; \
	fi
	$(echo) "(constituents.make) InDetPhysValMonitoringclean done"
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_makefile) InDetPhysValMonitoringclean

##	  /bin/rm -f $(cmt_local_InDetPhysValMonitoring_makefile) $(bin)InDetPhysValMonitoring_dependencies.make

install :: InDetPhysValMonitoringinstall ;

InDetPhysValMonitoringinstall :: $(InDetPhysValMonitoring_dependencies) $(cmt_local_InDetPhysValMonitoring_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_InDetPhysValMonitoring_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : InDetPhysValMonitoringuninstall

$(foreach d,$(InDetPhysValMonitoring_dependencies),$(eval $(d)uninstall_dependencies += InDetPhysValMonitoringuninstall))

InDetPhysValMonitoringuninstall : $(InDetPhysValMonitoringuninstall_dependencies) ##$(cmt_local_InDetPhysValMonitoring_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_InDetPhysValMonitoring_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: InDetPhysValMonitoringuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ InDetPhysValMonitoring"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ InDetPhysValMonitoring done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_install_python_modules_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_install_python_modules_has_target_tag

cmt_local_tagfile_install_python_modules = $(bin)$(InDetPhysValMonitoring_tag)_install_python_modules.make
cmt_final_setup_install_python_modules = $(bin)setup_install_python_modules.make
cmt_local_install_python_modules_makefile = $(bin)install_python_modules.make

install_python_modules_extratags = -tag_add=target_install_python_modules

else

cmt_local_tagfile_install_python_modules = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_install_python_modules = $(bin)setup.make
cmt_local_install_python_modules_makefile = $(bin)install_python_modules.make

endif

not_install_python_modules_dependencies = { n=0; for p in $?; do m=0; for d in $(install_python_modules_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
install_python_modulesdirs :
	@if test ! -d $(bin)install_python_modules; then $(mkdir) -p $(bin)install_python_modules; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)install_python_modules
else
install_python_modulesdirs : ;
endif

ifdef cmt_install_python_modules_has_target_tag

ifndef QUICK
$(cmt_local_install_python_modules_makefile) : $(install_python_modules_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_python_modules.make"; \
	  $(cmtexe) -tag=$(tags) $(install_python_modules_extratags) build constituent_config -out=$(cmt_local_install_python_modules_makefile) install_python_modules
else
$(cmt_local_install_python_modules_makefile) : $(install_python_modules_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_python_modules) ] || \
	  [ ! -f $(cmt_final_setup_install_python_modules) ] || \
	  $(not_install_python_modules_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_python_modules.make"; \
	  $(cmtexe) -tag=$(tags) $(install_python_modules_extratags) build constituent_config -out=$(cmt_local_install_python_modules_makefile) install_python_modules; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_install_python_modules_makefile) : $(install_python_modules_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_python_modules.make"; \
	  $(cmtexe) -f=$(bin)install_python_modules.in -tag=$(tags) $(install_python_modules_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_python_modules_makefile) install_python_modules
else
$(cmt_local_install_python_modules_makefile) : $(install_python_modules_dependencies) $(cmt_build_library_linksstamp) $(bin)install_python_modules.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_python_modules) ] || \
	  [ ! -f $(cmt_final_setup_install_python_modules) ] || \
	  $(not_install_python_modules_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_python_modules.make"; \
	  $(cmtexe) -f=$(bin)install_python_modules.in -tag=$(tags) $(install_python_modules_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_python_modules_makefile) install_python_modules; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(install_python_modules_extratags) build constituent_makefile -out=$(cmt_local_install_python_modules_makefile) install_python_modules

install_python_modules :: $(install_python_modules_dependencies) $(cmt_local_install_python_modules_makefile) dirs install_python_modulesdirs
	$(echo) "(constituents.make) Starting install_python_modules"
	@if test -f $(cmt_local_install_python_modules_makefile); then \
	  $(MAKE) -f $(cmt_local_install_python_modules_makefile) install_python_modules; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_python_modules_makefile) install_python_modules
	$(echo) "(constituents.make) install_python_modules done"

clean :: install_python_modulesclean ;

install_python_modulesclean :: $(install_python_modulesclean_dependencies) ##$(cmt_local_install_python_modules_makefile)
	$(echo) "(constituents.make) Starting install_python_modulesclean"
	@-if test -f $(cmt_local_install_python_modules_makefile); then \
	  $(MAKE) -f $(cmt_local_install_python_modules_makefile) install_python_modulesclean; \
	fi
	$(echo) "(constituents.make) install_python_modulesclean done"
#	@-$(MAKE) -f $(cmt_local_install_python_modules_makefile) install_python_modulesclean

##	  /bin/rm -f $(cmt_local_install_python_modules_makefile) $(bin)install_python_modules_dependencies.make

install :: install_python_modulesinstall ;

install_python_modulesinstall :: $(install_python_modules_dependencies) $(cmt_local_install_python_modules_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_install_python_modules_makefile); then \
	  $(MAKE) -f $(cmt_local_install_python_modules_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_install_python_modules_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : install_python_modulesuninstall

$(foreach d,$(install_python_modules_dependencies),$(eval $(d)uninstall_dependencies += install_python_modulesuninstall))

install_python_modulesuninstall : $(install_python_modulesuninstall_dependencies) ##$(cmt_local_install_python_modules_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_install_python_modules_makefile); then \
	  $(MAKE) -f $(cmt_local_install_python_modules_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_python_modules_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: install_python_modulesuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ install_python_modules"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ install_python_modules done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_InDetPhysValMonitoringConf_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_InDetPhysValMonitoringConf_has_target_tag

cmt_local_tagfile_InDetPhysValMonitoringConf = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringConf.make
cmt_final_setup_InDetPhysValMonitoringConf = $(bin)setup_InDetPhysValMonitoringConf.make
cmt_local_InDetPhysValMonitoringConf_makefile = $(bin)InDetPhysValMonitoringConf.make

InDetPhysValMonitoringConf_extratags = -tag_add=target_InDetPhysValMonitoringConf

else

cmt_local_tagfile_InDetPhysValMonitoringConf = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_InDetPhysValMonitoringConf = $(bin)setup.make
cmt_local_InDetPhysValMonitoringConf_makefile = $(bin)InDetPhysValMonitoringConf.make

endif

not_InDetPhysValMonitoringConf_dependencies = { n=0; for p in $?; do m=0; for d in $(InDetPhysValMonitoringConf_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
InDetPhysValMonitoringConfdirs :
	@if test ! -d $(bin)InDetPhysValMonitoringConf; then $(mkdir) -p $(bin)InDetPhysValMonitoringConf; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)InDetPhysValMonitoringConf
else
InDetPhysValMonitoringConfdirs : ;
endif

ifdef cmt_InDetPhysValMonitoringConf_has_target_tag

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringConf_makefile) : $(InDetPhysValMonitoringConf_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringConf.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringConf_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringConf_makefile) InDetPhysValMonitoringConf
else
$(cmt_local_InDetPhysValMonitoringConf_makefile) : $(InDetPhysValMonitoringConf_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringConf) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringConf) ] || \
	  $(not_InDetPhysValMonitoringConf_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringConf.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringConf_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringConf_makefile) InDetPhysValMonitoringConf; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringConf_makefile) : $(InDetPhysValMonitoringConf_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringConf.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringConf.in -tag=$(tags) $(InDetPhysValMonitoringConf_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringConf_makefile) InDetPhysValMonitoringConf
else
$(cmt_local_InDetPhysValMonitoringConf_makefile) : $(InDetPhysValMonitoringConf_dependencies) $(cmt_build_library_linksstamp) $(bin)InDetPhysValMonitoringConf.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringConf) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringConf) ] || \
	  $(not_InDetPhysValMonitoringConf_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringConf.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringConf.in -tag=$(tags) $(InDetPhysValMonitoringConf_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringConf_makefile) InDetPhysValMonitoringConf; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringConf_extratags) build constituent_makefile -out=$(cmt_local_InDetPhysValMonitoringConf_makefile) InDetPhysValMonitoringConf

InDetPhysValMonitoringConf :: $(InDetPhysValMonitoringConf_dependencies) $(cmt_local_InDetPhysValMonitoringConf_makefile) dirs InDetPhysValMonitoringConfdirs
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringConf"
	@if test -f $(cmt_local_InDetPhysValMonitoringConf_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringConf_makefile) InDetPhysValMonitoringConf; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringConf_makefile) InDetPhysValMonitoringConf
	$(echo) "(constituents.make) InDetPhysValMonitoringConf done"

clean :: InDetPhysValMonitoringConfclean ;

InDetPhysValMonitoringConfclean :: $(InDetPhysValMonitoringConfclean_dependencies) ##$(cmt_local_InDetPhysValMonitoringConf_makefile)
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringConfclean"
	@-if test -f $(cmt_local_InDetPhysValMonitoringConf_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringConf_makefile) InDetPhysValMonitoringConfclean; \
	fi
	$(echo) "(constituents.make) InDetPhysValMonitoringConfclean done"
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringConf_makefile) InDetPhysValMonitoringConfclean

##	  /bin/rm -f $(cmt_local_InDetPhysValMonitoringConf_makefile) $(bin)InDetPhysValMonitoringConf_dependencies.make

install :: InDetPhysValMonitoringConfinstall ;

InDetPhysValMonitoringConfinstall :: $(InDetPhysValMonitoringConf_dependencies) $(cmt_local_InDetPhysValMonitoringConf_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_InDetPhysValMonitoringConf_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringConf_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringConf_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : InDetPhysValMonitoringConfuninstall

$(foreach d,$(InDetPhysValMonitoringConf_dependencies),$(eval $(d)uninstall_dependencies += InDetPhysValMonitoringConfuninstall))

InDetPhysValMonitoringConfuninstall : $(InDetPhysValMonitoringConfuninstall_dependencies) ##$(cmt_local_InDetPhysValMonitoringConf_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_InDetPhysValMonitoringConf_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringConf_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringConf_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: InDetPhysValMonitoringConfuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ InDetPhysValMonitoringConf"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ InDetPhysValMonitoringConf done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_InDetPhysValMonitoring_python_init_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_InDetPhysValMonitoring_python_init_has_target_tag

cmt_local_tagfile_InDetPhysValMonitoring_python_init = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoring_python_init.make
cmt_final_setup_InDetPhysValMonitoring_python_init = $(bin)setup_InDetPhysValMonitoring_python_init.make
cmt_local_InDetPhysValMonitoring_python_init_makefile = $(bin)InDetPhysValMonitoring_python_init.make

InDetPhysValMonitoring_python_init_extratags = -tag_add=target_InDetPhysValMonitoring_python_init

else

cmt_local_tagfile_InDetPhysValMonitoring_python_init = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_InDetPhysValMonitoring_python_init = $(bin)setup.make
cmt_local_InDetPhysValMonitoring_python_init_makefile = $(bin)InDetPhysValMonitoring_python_init.make

endif

not_InDetPhysValMonitoring_python_init_dependencies = { n=0; for p in $?; do m=0; for d in $(InDetPhysValMonitoring_python_init_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
InDetPhysValMonitoring_python_initdirs :
	@if test ! -d $(bin)InDetPhysValMonitoring_python_init; then $(mkdir) -p $(bin)InDetPhysValMonitoring_python_init; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)InDetPhysValMonitoring_python_init
else
InDetPhysValMonitoring_python_initdirs : ;
endif

ifdef cmt_InDetPhysValMonitoring_python_init_has_target_tag

ifndef QUICK
$(cmt_local_InDetPhysValMonitoring_python_init_makefile) : $(InDetPhysValMonitoring_python_init_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoring_python_init.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_python_init_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoring_python_init_makefile) InDetPhysValMonitoring_python_init
else
$(cmt_local_InDetPhysValMonitoring_python_init_makefile) : $(InDetPhysValMonitoring_python_init_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoring_python_init) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoring_python_init) ] || \
	  $(not_InDetPhysValMonitoring_python_init_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoring_python_init.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_python_init_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoring_python_init_makefile) InDetPhysValMonitoring_python_init; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_InDetPhysValMonitoring_python_init_makefile) : $(InDetPhysValMonitoring_python_init_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoring_python_init.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoring_python_init.in -tag=$(tags) $(InDetPhysValMonitoring_python_init_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoring_python_init_makefile) InDetPhysValMonitoring_python_init
else
$(cmt_local_InDetPhysValMonitoring_python_init_makefile) : $(InDetPhysValMonitoring_python_init_dependencies) $(cmt_build_library_linksstamp) $(bin)InDetPhysValMonitoring_python_init.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoring_python_init) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoring_python_init) ] || \
	  $(not_InDetPhysValMonitoring_python_init_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoring_python_init.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoring_python_init.in -tag=$(tags) $(InDetPhysValMonitoring_python_init_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoring_python_init_makefile) InDetPhysValMonitoring_python_init; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_python_init_extratags) build constituent_makefile -out=$(cmt_local_InDetPhysValMonitoring_python_init_makefile) InDetPhysValMonitoring_python_init

InDetPhysValMonitoring_python_init :: $(InDetPhysValMonitoring_python_init_dependencies) $(cmt_local_InDetPhysValMonitoring_python_init_makefile) dirs InDetPhysValMonitoring_python_initdirs
	$(echo) "(constituents.make) Starting InDetPhysValMonitoring_python_init"
	@if test -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile) InDetPhysValMonitoring_python_init; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile) InDetPhysValMonitoring_python_init
	$(echo) "(constituents.make) InDetPhysValMonitoring_python_init done"

clean :: InDetPhysValMonitoring_python_initclean ;

InDetPhysValMonitoring_python_initclean :: $(InDetPhysValMonitoring_python_initclean_dependencies) ##$(cmt_local_InDetPhysValMonitoring_python_init_makefile)
	$(echo) "(constituents.make) Starting InDetPhysValMonitoring_python_initclean"
	@-if test -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile) InDetPhysValMonitoring_python_initclean; \
	fi
	$(echo) "(constituents.make) InDetPhysValMonitoring_python_initclean done"
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile) InDetPhysValMonitoring_python_initclean

##	  /bin/rm -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile) $(bin)InDetPhysValMonitoring_python_init_dependencies.make

install :: InDetPhysValMonitoring_python_initinstall ;

InDetPhysValMonitoring_python_initinstall :: $(InDetPhysValMonitoring_python_init_dependencies) $(cmt_local_InDetPhysValMonitoring_python_init_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : InDetPhysValMonitoring_python_inituninstall

$(foreach d,$(InDetPhysValMonitoring_python_init_dependencies),$(eval $(d)uninstall_dependencies += InDetPhysValMonitoring_python_inituninstall))

InDetPhysValMonitoring_python_inituninstall : $(InDetPhysValMonitoring_python_inituninstall_dependencies) ##$(cmt_local_InDetPhysValMonitoring_python_init_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_python_init_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: InDetPhysValMonitoring_python_inituninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ InDetPhysValMonitoring_python_init"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ InDetPhysValMonitoring_python_init done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_InDetPhysValMonitoringConfDbMerge_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_InDetPhysValMonitoringConfDbMerge_has_target_tag

cmt_local_tagfile_InDetPhysValMonitoringConfDbMerge = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringConfDbMerge.make
cmt_final_setup_InDetPhysValMonitoringConfDbMerge = $(bin)setup_InDetPhysValMonitoringConfDbMerge.make
cmt_local_InDetPhysValMonitoringConfDbMerge_makefile = $(bin)InDetPhysValMonitoringConfDbMerge.make

InDetPhysValMonitoringConfDbMerge_extratags = -tag_add=target_InDetPhysValMonitoringConfDbMerge

else

cmt_local_tagfile_InDetPhysValMonitoringConfDbMerge = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_InDetPhysValMonitoringConfDbMerge = $(bin)setup.make
cmt_local_InDetPhysValMonitoringConfDbMerge_makefile = $(bin)InDetPhysValMonitoringConfDbMerge.make

endif

not_InDetPhysValMonitoringConfDbMerge_dependencies = { n=0; for p in $?; do m=0; for d in $(InDetPhysValMonitoringConfDbMerge_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
InDetPhysValMonitoringConfDbMergedirs :
	@if test ! -d $(bin)InDetPhysValMonitoringConfDbMerge; then $(mkdir) -p $(bin)InDetPhysValMonitoringConfDbMerge; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)InDetPhysValMonitoringConfDbMerge
else
InDetPhysValMonitoringConfDbMergedirs : ;
endif

ifdef cmt_InDetPhysValMonitoringConfDbMerge_has_target_tag

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) : $(InDetPhysValMonitoringConfDbMerge_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringConfDbMerge.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringConfDbMerge_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) InDetPhysValMonitoringConfDbMerge
else
$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) : $(InDetPhysValMonitoringConfDbMerge_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringConfDbMerge) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringConfDbMerge) ] || \
	  $(not_InDetPhysValMonitoringConfDbMerge_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringConfDbMerge.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringConfDbMerge_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) InDetPhysValMonitoringConfDbMerge; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) : $(InDetPhysValMonitoringConfDbMerge_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringConfDbMerge.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringConfDbMerge.in -tag=$(tags) $(InDetPhysValMonitoringConfDbMerge_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) InDetPhysValMonitoringConfDbMerge
else
$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) : $(InDetPhysValMonitoringConfDbMerge_dependencies) $(cmt_build_library_linksstamp) $(bin)InDetPhysValMonitoringConfDbMerge.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringConfDbMerge) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringConfDbMerge) ] || \
	  $(not_InDetPhysValMonitoringConfDbMerge_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringConfDbMerge.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringConfDbMerge.in -tag=$(tags) $(InDetPhysValMonitoringConfDbMerge_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) InDetPhysValMonitoringConfDbMerge; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringConfDbMerge_extratags) build constituent_makefile -out=$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) InDetPhysValMonitoringConfDbMerge

InDetPhysValMonitoringConfDbMerge :: $(InDetPhysValMonitoringConfDbMerge_dependencies) $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) dirs InDetPhysValMonitoringConfDbMergedirs
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringConfDbMerge"
	@if test -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) InDetPhysValMonitoringConfDbMerge; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) InDetPhysValMonitoringConfDbMerge
	$(echo) "(constituents.make) InDetPhysValMonitoringConfDbMerge done"

clean :: InDetPhysValMonitoringConfDbMergeclean ;

InDetPhysValMonitoringConfDbMergeclean :: $(InDetPhysValMonitoringConfDbMergeclean_dependencies) ##$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile)
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringConfDbMergeclean"
	@-if test -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) InDetPhysValMonitoringConfDbMergeclean; \
	fi
	$(echo) "(constituents.make) InDetPhysValMonitoringConfDbMergeclean done"
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) InDetPhysValMonitoringConfDbMergeclean

##	  /bin/rm -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) $(bin)InDetPhysValMonitoringConfDbMerge_dependencies.make

install :: InDetPhysValMonitoringConfDbMergeinstall ;

InDetPhysValMonitoringConfDbMergeinstall :: $(InDetPhysValMonitoringConfDbMerge_dependencies) $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : InDetPhysValMonitoringConfDbMergeuninstall

$(foreach d,$(InDetPhysValMonitoringConfDbMerge_dependencies),$(eval $(d)uninstall_dependencies += InDetPhysValMonitoringConfDbMergeuninstall))

InDetPhysValMonitoringConfDbMergeuninstall : $(InDetPhysValMonitoringConfDbMergeuninstall_dependencies) ##$(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringConfDbMerge_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: InDetPhysValMonitoringConfDbMergeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ InDetPhysValMonitoringConfDbMerge"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ InDetPhysValMonitoringConfDbMerge done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_InDetPhysValMonitoringComponentsList_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_InDetPhysValMonitoringComponentsList_has_target_tag

cmt_local_tagfile_InDetPhysValMonitoringComponentsList = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringComponentsList.make
cmt_final_setup_InDetPhysValMonitoringComponentsList = $(bin)setup_InDetPhysValMonitoringComponentsList.make
cmt_local_InDetPhysValMonitoringComponentsList_makefile = $(bin)InDetPhysValMonitoringComponentsList.make

InDetPhysValMonitoringComponentsList_extratags = -tag_add=target_InDetPhysValMonitoringComponentsList

else

cmt_local_tagfile_InDetPhysValMonitoringComponentsList = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_InDetPhysValMonitoringComponentsList = $(bin)setup.make
cmt_local_InDetPhysValMonitoringComponentsList_makefile = $(bin)InDetPhysValMonitoringComponentsList.make

endif

not_InDetPhysValMonitoringComponentsList_dependencies = { n=0; for p in $?; do m=0; for d in $(InDetPhysValMonitoringComponentsList_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
InDetPhysValMonitoringComponentsListdirs :
	@if test ! -d $(bin)InDetPhysValMonitoringComponentsList; then $(mkdir) -p $(bin)InDetPhysValMonitoringComponentsList; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)InDetPhysValMonitoringComponentsList
else
InDetPhysValMonitoringComponentsListdirs : ;
endif

ifdef cmt_InDetPhysValMonitoringComponentsList_has_target_tag

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringComponentsList_makefile) : $(InDetPhysValMonitoringComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringComponentsList_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringComponentsList_makefile) InDetPhysValMonitoringComponentsList
else
$(cmt_local_InDetPhysValMonitoringComponentsList_makefile) : $(InDetPhysValMonitoringComponentsList_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringComponentsList) ] || \
	  $(not_InDetPhysValMonitoringComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringComponentsList_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringComponentsList_makefile) InDetPhysValMonitoringComponentsList; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringComponentsList_makefile) : $(InDetPhysValMonitoringComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringComponentsList.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringComponentsList.in -tag=$(tags) $(InDetPhysValMonitoringComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringComponentsList_makefile) InDetPhysValMonitoringComponentsList
else
$(cmt_local_InDetPhysValMonitoringComponentsList_makefile) : $(InDetPhysValMonitoringComponentsList_dependencies) $(cmt_build_library_linksstamp) $(bin)InDetPhysValMonitoringComponentsList.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringComponentsList) ] || \
	  $(not_InDetPhysValMonitoringComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringComponentsList.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringComponentsList.in -tag=$(tags) $(InDetPhysValMonitoringComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringComponentsList_makefile) InDetPhysValMonitoringComponentsList; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringComponentsList_extratags) build constituent_makefile -out=$(cmt_local_InDetPhysValMonitoringComponentsList_makefile) InDetPhysValMonitoringComponentsList

InDetPhysValMonitoringComponentsList :: $(InDetPhysValMonitoringComponentsList_dependencies) $(cmt_local_InDetPhysValMonitoringComponentsList_makefile) dirs InDetPhysValMonitoringComponentsListdirs
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringComponentsList"
	@if test -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile) InDetPhysValMonitoringComponentsList; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile) InDetPhysValMonitoringComponentsList
	$(echo) "(constituents.make) InDetPhysValMonitoringComponentsList done"

clean :: InDetPhysValMonitoringComponentsListclean ;

InDetPhysValMonitoringComponentsListclean :: $(InDetPhysValMonitoringComponentsListclean_dependencies) ##$(cmt_local_InDetPhysValMonitoringComponentsList_makefile)
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringComponentsListclean"
	@-if test -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile) InDetPhysValMonitoringComponentsListclean; \
	fi
	$(echo) "(constituents.make) InDetPhysValMonitoringComponentsListclean done"
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile) InDetPhysValMonitoringComponentsListclean

##	  /bin/rm -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile) $(bin)InDetPhysValMonitoringComponentsList_dependencies.make

install :: InDetPhysValMonitoringComponentsListinstall ;

InDetPhysValMonitoringComponentsListinstall :: $(InDetPhysValMonitoringComponentsList_dependencies) $(cmt_local_InDetPhysValMonitoringComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : InDetPhysValMonitoringComponentsListuninstall

$(foreach d,$(InDetPhysValMonitoringComponentsList_dependencies),$(eval $(d)uninstall_dependencies += InDetPhysValMonitoringComponentsListuninstall))

InDetPhysValMonitoringComponentsListuninstall : $(InDetPhysValMonitoringComponentsListuninstall_dependencies) ##$(cmt_local_InDetPhysValMonitoringComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringComponentsList_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: InDetPhysValMonitoringComponentsListuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ InDetPhysValMonitoringComponentsList"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ InDetPhysValMonitoringComponentsList done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_InDetPhysValMonitoringMergeComponentsList_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_InDetPhysValMonitoringMergeComponentsList_has_target_tag

cmt_local_tagfile_InDetPhysValMonitoringMergeComponentsList = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringMergeComponentsList.make
cmt_final_setup_InDetPhysValMonitoringMergeComponentsList = $(bin)setup_InDetPhysValMonitoringMergeComponentsList.make
cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile = $(bin)InDetPhysValMonitoringMergeComponentsList.make

InDetPhysValMonitoringMergeComponentsList_extratags = -tag_add=target_InDetPhysValMonitoringMergeComponentsList

else

cmt_local_tagfile_InDetPhysValMonitoringMergeComponentsList = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_InDetPhysValMonitoringMergeComponentsList = $(bin)setup.make
cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile = $(bin)InDetPhysValMonitoringMergeComponentsList.make

endif

not_InDetPhysValMonitoringMergeComponentsList_dependencies = { n=0; for p in $?; do m=0; for d in $(InDetPhysValMonitoringMergeComponentsList_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
InDetPhysValMonitoringMergeComponentsListdirs :
	@if test ! -d $(bin)InDetPhysValMonitoringMergeComponentsList; then $(mkdir) -p $(bin)InDetPhysValMonitoringMergeComponentsList; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)InDetPhysValMonitoringMergeComponentsList
else
InDetPhysValMonitoringMergeComponentsListdirs : ;
endif

ifdef cmt_InDetPhysValMonitoringMergeComponentsList_has_target_tag

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) : $(InDetPhysValMonitoringMergeComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringMergeComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringMergeComponentsList_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) InDetPhysValMonitoringMergeComponentsList
else
$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) : $(InDetPhysValMonitoringMergeComponentsList_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringMergeComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringMergeComponentsList) ] || \
	  $(not_InDetPhysValMonitoringMergeComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringMergeComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringMergeComponentsList_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) InDetPhysValMonitoringMergeComponentsList; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) : $(InDetPhysValMonitoringMergeComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringMergeComponentsList.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringMergeComponentsList.in -tag=$(tags) $(InDetPhysValMonitoringMergeComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) InDetPhysValMonitoringMergeComponentsList
else
$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) : $(InDetPhysValMonitoringMergeComponentsList_dependencies) $(cmt_build_library_linksstamp) $(bin)InDetPhysValMonitoringMergeComponentsList.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringMergeComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringMergeComponentsList) ] || \
	  $(not_InDetPhysValMonitoringMergeComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringMergeComponentsList.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringMergeComponentsList.in -tag=$(tags) $(InDetPhysValMonitoringMergeComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) InDetPhysValMonitoringMergeComponentsList; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringMergeComponentsList_extratags) build constituent_makefile -out=$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) InDetPhysValMonitoringMergeComponentsList

InDetPhysValMonitoringMergeComponentsList :: $(InDetPhysValMonitoringMergeComponentsList_dependencies) $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) dirs InDetPhysValMonitoringMergeComponentsListdirs
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringMergeComponentsList"
	@if test -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) InDetPhysValMonitoringMergeComponentsList; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) InDetPhysValMonitoringMergeComponentsList
	$(echo) "(constituents.make) InDetPhysValMonitoringMergeComponentsList done"

clean :: InDetPhysValMonitoringMergeComponentsListclean ;

InDetPhysValMonitoringMergeComponentsListclean :: $(InDetPhysValMonitoringMergeComponentsListclean_dependencies) ##$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile)
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringMergeComponentsListclean"
	@-if test -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) InDetPhysValMonitoringMergeComponentsListclean; \
	fi
	$(echo) "(constituents.make) InDetPhysValMonitoringMergeComponentsListclean done"
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) InDetPhysValMonitoringMergeComponentsListclean

##	  /bin/rm -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) $(bin)InDetPhysValMonitoringMergeComponentsList_dependencies.make

install :: InDetPhysValMonitoringMergeComponentsListinstall ;

InDetPhysValMonitoringMergeComponentsListinstall :: $(InDetPhysValMonitoringMergeComponentsList_dependencies) $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : InDetPhysValMonitoringMergeComponentsListuninstall

$(foreach d,$(InDetPhysValMonitoringMergeComponentsList_dependencies),$(eval $(d)uninstall_dependencies += InDetPhysValMonitoringMergeComponentsListuninstall))

InDetPhysValMonitoringMergeComponentsListuninstall : $(InDetPhysValMonitoringMergeComponentsListuninstall_dependencies) ##$(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringMergeComponentsList_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: InDetPhysValMonitoringMergeComponentsListuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ InDetPhysValMonitoringMergeComponentsList"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ InDetPhysValMonitoringMergeComponentsList done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_InDetPhysValMonitoring_optdebug_library_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_InDetPhysValMonitoring_optdebug_library_has_target_tag

cmt_local_tagfile_InDetPhysValMonitoring_optdebug_library = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoring_optdebug_library.make
cmt_final_setup_InDetPhysValMonitoring_optdebug_library = $(bin)setup_InDetPhysValMonitoring_optdebug_library.make
cmt_local_InDetPhysValMonitoring_optdebug_library_makefile = $(bin)InDetPhysValMonitoring_optdebug_library.make

InDetPhysValMonitoring_optdebug_library_extratags = -tag_add=target_InDetPhysValMonitoring_optdebug_library

else

cmt_local_tagfile_InDetPhysValMonitoring_optdebug_library = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_InDetPhysValMonitoring_optdebug_library = $(bin)setup.make
cmt_local_InDetPhysValMonitoring_optdebug_library_makefile = $(bin)InDetPhysValMonitoring_optdebug_library.make

endif

not_InDetPhysValMonitoring_optdebug_library_dependencies = { n=0; for p in $?; do m=0; for d in $(InDetPhysValMonitoring_optdebug_library_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
InDetPhysValMonitoring_optdebug_librarydirs :
	@if test ! -d $(bin)InDetPhysValMonitoring_optdebug_library; then $(mkdir) -p $(bin)InDetPhysValMonitoring_optdebug_library; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)InDetPhysValMonitoring_optdebug_library
else
InDetPhysValMonitoring_optdebug_librarydirs : ;
endif

ifdef cmt_InDetPhysValMonitoring_optdebug_library_has_target_tag

ifndef QUICK
$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) : $(InDetPhysValMonitoring_optdebug_library_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoring_optdebug_library.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_optdebug_library_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) InDetPhysValMonitoring_optdebug_library
else
$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) : $(InDetPhysValMonitoring_optdebug_library_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoring_optdebug_library) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoring_optdebug_library) ] || \
	  $(not_InDetPhysValMonitoring_optdebug_library_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoring_optdebug_library.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_optdebug_library_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) InDetPhysValMonitoring_optdebug_library; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) : $(InDetPhysValMonitoring_optdebug_library_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoring_optdebug_library.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoring_optdebug_library.in -tag=$(tags) $(InDetPhysValMonitoring_optdebug_library_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) InDetPhysValMonitoring_optdebug_library
else
$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) : $(InDetPhysValMonitoring_optdebug_library_dependencies) $(cmt_build_library_linksstamp) $(bin)InDetPhysValMonitoring_optdebug_library.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoring_optdebug_library) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoring_optdebug_library) ] || \
	  $(not_InDetPhysValMonitoring_optdebug_library_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoring_optdebug_library.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoring_optdebug_library.in -tag=$(tags) $(InDetPhysValMonitoring_optdebug_library_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) InDetPhysValMonitoring_optdebug_library; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_optdebug_library_extratags) build constituent_makefile -out=$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) InDetPhysValMonitoring_optdebug_library

InDetPhysValMonitoring_optdebug_library :: $(InDetPhysValMonitoring_optdebug_library_dependencies) $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) dirs InDetPhysValMonitoring_optdebug_librarydirs
	$(echo) "(constituents.make) Starting InDetPhysValMonitoring_optdebug_library"
	@if test -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) InDetPhysValMonitoring_optdebug_library; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) InDetPhysValMonitoring_optdebug_library
	$(echo) "(constituents.make) InDetPhysValMonitoring_optdebug_library done"

clean :: InDetPhysValMonitoring_optdebug_libraryclean ;

InDetPhysValMonitoring_optdebug_libraryclean :: $(InDetPhysValMonitoring_optdebug_libraryclean_dependencies) ##$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile)
	$(echo) "(constituents.make) Starting InDetPhysValMonitoring_optdebug_libraryclean"
	@-if test -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) InDetPhysValMonitoring_optdebug_libraryclean; \
	fi
	$(echo) "(constituents.make) InDetPhysValMonitoring_optdebug_libraryclean done"
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) InDetPhysValMonitoring_optdebug_libraryclean

##	  /bin/rm -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) $(bin)InDetPhysValMonitoring_optdebug_library_dependencies.make

install :: InDetPhysValMonitoring_optdebug_libraryinstall ;

InDetPhysValMonitoring_optdebug_libraryinstall :: $(InDetPhysValMonitoring_optdebug_library_dependencies) $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : InDetPhysValMonitoring_optdebug_libraryuninstall

$(foreach d,$(InDetPhysValMonitoring_optdebug_library_dependencies),$(eval $(d)uninstall_dependencies += InDetPhysValMonitoring_optdebug_libraryuninstall))

InDetPhysValMonitoring_optdebug_libraryuninstall : $(InDetPhysValMonitoring_optdebug_libraryuninstall_dependencies) ##$(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_optdebug_library_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: InDetPhysValMonitoring_optdebug_libraryuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ InDetPhysValMonitoring_optdebug_library"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ InDetPhysValMonitoring_optdebug_library done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_InDetPhysValMonitoringCLIDDB_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_InDetPhysValMonitoringCLIDDB_has_target_tag

cmt_local_tagfile_InDetPhysValMonitoringCLIDDB = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringCLIDDB.make
cmt_final_setup_InDetPhysValMonitoringCLIDDB = $(bin)setup_InDetPhysValMonitoringCLIDDB.make
cmt_local_InDetPhysValMonitoringCLIDDB_makefile = $(bin)InDetPhysValMonitoringCLIDDB.make

InDetPhysValMonitoringCLIDDB_extratags = -tag_add=target_InDetPhysValMonitoringCLIDDB

else

cmt_local_tagfile_InDetPhysValMonitoringCLIDDB = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_InDetPhysValMonitoringCLIDDB = $(bin)setup.make
cmt_local_InDetPhysValMonitoringCLIDDB_makefile = $(bin)InDetPhysValMonitoringCLIDDB.make

endif

not_InDetPhysValMonitoringCLIDDB_dependencies = { n=0; for p in $?; do m=0; for d in $(InDetPhysValMonitoringCLIDDB_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
InDetPhysValMonitoringCLIDDBdirs :
	@if test ! -d $(bin)InDetPhysValMonitoringCLIDDB; then $(mkdir) -p $(bin)InDetPhysValMonitoringCLIDDB; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)InDetPhysValMonitoringCLIDDB
else
InDetPhysValMonitoringCLIDDBdirs : ;
endif

ifdef cmt_InDetPhysValMonitoringCLIDDB_has_target_tag

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) : $(InDetPhysValMonitoringCLIDDB_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringCLIDDB.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringCLIDDB_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) InDetPhysValMonitoringCLIDDB
else
$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) : $(InDetPhysValMonitoringCLIDDB_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringCLIDDB) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringCLIDDB) ] || \
	  $(not_InDetPhysValMonitoringCLIDDB_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringCLIDDB.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringCLIDDB_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) InDetPhysValMonitoringCLIDDB; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) : $(InDetPhysValMonitoringCLIDDB_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringCLIDDB.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringCLIDDB.in -tag=$(tags) $(InDetPhysValMonitoringCLIDDB_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) InDetPhysValMonitoringCLIDDB
else
$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) : $(InDetPhysValMonitoringCLIDDB_dependencies) $(cmt_build_library_linksstamp) $(bin)InDetPhysValMonitoringCLIDDB.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringCLIDDB) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringCLIDDB) ] || \
	  $(not_InDetPhysValMonitoringCLIDDB_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringCLIDDB.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringCLIDDB.in -tag=$(tags) $(InDetPhysValMonitoringCLIDDB_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) InDetPhysValMonitoringCLIDDB; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringCLIDDB_extratags) build constituent_makefile -out=$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) InDetPhysValMonitoringCLIDDB

InDetPhysValMonitoringCLIDDB :: $(InDetPhysValMonitoringCLIDDB_dependencies) $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) dirs InDetPhysValMonitoringCLIDDBdirs
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringCLIDDB"
	@if test -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) InDetPhysValMonitoringCLIDDB; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) InDetPhysValMonitoringCLIDDB
	$(echo) "(constituents.make) InDetPhysValMonitoringCLIDDB done"

clean :: InDetPhysValMonitoringCLIDDBclean ;

InDetPhysValMonitoringCLIDDBclean :: $(InDetPhysValMonitoringCLIDDBclean_dependencies) ##$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile)
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringCLIDDBclean"
	@-if test -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) InDetPhysValMonitoringCLIDDBclean; \
	fi
	$(echo) "(constituents.make) InDetPhysValMonitoringCLIDDBclean done"
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) InDetPhysValMonitoringCLIDDBclean

##	  /bin/rm -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) $(bin)InDetPhysValMonitoringCLIDDB_dependencies.make

install :: InDetPhysValMonitoringCLIDDBinstall ;

InDetPhysValMonitoringCLIDDBinstall :: $(InDetPhysValMonitoringCLIDDB_dependencies) $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : InDetPhysValMonitoringCLIDDBuninstall

$(foreach d,$(InDetPhysValMonitoringCLIDDB_dependencies),$(eval $(d)uninstall_dependencies += InDetPhysValMonitoringCLIDDBuninstall))

InDetPhysValMonitoringCLIDDBuninstall : $(InDetPhysValMonitoringCLIDDBuninstall_dependencies) ##$(cmt_local_InDetPhysValMonitoringCLIDDB_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringCLIDDB_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: InDetPhysValMonitoringCLIDDBuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ InDetPhysValMonitoringCLIDDB"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ InDetPhysValMonitoringCLIDDB done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_InDetPhysValMonitoringrchk_has_target_tag = 1

#--------------------------------------

ifdef cmt_InDetPhysValMonitoringrchk_has_target_tag

cmt_local_tagfile_InDetPhysValMonitoringrchk = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoringrchk.make
cmt_final_setup_InDetPhysValMonitoringrchk = $(bin)setup_InDetPhysValMonitoringrchk.make
cmt_local_InDetPhysValMonitoringrchk_makefile = $(bin)InDetPhysValMonitoringrchk.make

InDetPhysValMonitoringrchk_extratags = -tag_add=target_InDetPhysValMonitoringrchk

else

cmt_local_tagfile_InDetPhysValMonitoringrchk = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_InDetPhysValMonitoringrchk = $(bin)setup.make
cmt_local_InDetPhysValMonitoringrchk_makefile = $(bin)InDetPhysValMonitoringrchk.make

endif

not_InDetPhysValMonitoringrchk_dependencies = { n=0; for p in $?; do m=0; for d in $(InDetPhysValMonitoringrchk_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
InDetPhysValMonitoringrchkdirs :
	@if test ! -d $(bin)InDetPhysValMonitoringrchk; then $(mkdir) -p $(bin)InDetPhysValMonitoringrchk; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)InDetPhysValMonitoringrchk
else
InDetPhysValMonitoringrchkdirs : ;
endif

ifdef cmt_InDetPhysValMonitoringrchk_has_target_tag

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringrchk_makefile) : $(InDetPhysValMonitoringrchk_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringrchk.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringrchk_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringrchk_makefile) InDetPhysValMonitoringrchk
else
$(cmt_local_InDetPhysValMonitoringrchk_makefile) : $(InDetPhysValMonitoringrchk_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringrchk) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringrchk) ] || \
	  $(not_InDetPhysValMonitoringrchk_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringrchk.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringrchk_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoringrchk_makefile) InDetPhysValMonitoringrchk; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_InDetPhysValMonitoringrchk_makefile) : $(InDetPhysValMonitoringrchk_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoringrchk.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringrchk.in -tag=$(tags) $(InDetPhysValMonitoringrchk_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringrchk_makefile) InDetPhysValMonitoringrchk
else
$(cmt_local_InDetPhysValMonitoringrchk_makefile) : $(InDetPhysValMonitoringrchk_dependencies) $(cmt_build_library_linksstamp) $(bin)InDetPhysValMonitoringrchk.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoringrchk) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoringrchk) ] || \
	  $(not_InDetPhysValMonitoringrchk_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoringrchk.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoringrchk.in -tag=$(tags) $(InDetPhysValMonitoringrchk_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoringrchk_makefile) InDetPhysValMonitoringrchk; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoringrchk_extratags) build constituent_makefile -out=$(cmt_local_InDetPhysValMonitoringrchk_makefile) InDetPhysValMonitoringrchk

InDetPhysValMonitoringrchk :: $(InDetPhysValMonitoringrchk_dependencies) $(cmt_local_InDetPhysValMonitoringrchk_makefile) dirs InDetPhysValMonitoringrchkdirs
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringrchk"
	@if test -f $(cmt_local_InDetPhysValMonitoringrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringrchk_makefile) InDetPhysValMonitoringrchk; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringrchk_makefile) InDetPhysValMonitoringrchk
	$(echo) "(constituents.make) InDetPhysValMonitoringrchk done"

clean :: InDetPhysValMonitoringrchkclean ;

InDetPhysValMonitoringrchkclean :: $(InDetPhysValMonitoringrchkclean_dependencies) ##$(cmt_local_InDetPhysValMonitoringrchk_makefile)
	$(echo) "(constituents.make) Starting InDetPhysValMonitoringrchkclean"
	@-if test -f $(cmt_local_InDetPhysValMonitoringrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringrchk_makefile) InDetPhysValMonitoringrchkclean; \
	fi
	$(echo) "(constituents.make) InDetPhysValMonitoringrchkclean done"
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringrchk_makefile) InDetPhysValMonitoringrchkclean

##	  /bin/rm -f $(cmt_local_InDetPhysValMonitoringrchk_makefile) $(bin)InDetPhysValMonitoringrchk_dependencies.make

install :: InDetPhysValMonitoringrchkinstall ;

InDetPhysValMonitoringrchkinstall :: $(InDetPhysValMonitoringrchk_dependencies) $(cmt_local_InDetPhysValMonitoringrchk_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_InDetPhysValMonitoringrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringrchk_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoringrchk_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : InDetPhysValMonitoringrchkuninstall

$(foreach d,$(InDetPhysValMonitoringrchk_dependencies),$(eval $(d)uninstall_dependencies += InDetPhysValMonitoringrchkuninstall))

InDetPhysValMonitoringrchkuninstall : $(InDetPhysValMonitoringrchkuninstall_dependencies) ##$(cmt_local_InDetPhysValMonitoringrchk_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_InDetPhysValMonitoringrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoringrchk_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoringrchk_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: InDetPhysValMonitoringrchkuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ InDetPhysValMonitoringrchk"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ InDetPhysValMonitoringrchk done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_install_root_include_path_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_install_root_include_path_has_target_tag

cmt_local_tagfile_install_root_include_path = $(bin)$(InDetPhysValMonitoring_tag)_install_root_include_path.make
cmt_final_setup_install_root_include_path = $(bin)setup_install_root_include_path.make
cmt_local_install_root_include_path_makefile = $(bin)install_root_include_path.make

install_root_include_path_extratags = -tag_add=target_install_root_include_path

else

cmt_local_tagfile_install_root_include_path = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_install_root_include_path = $(bin)setup.make
cmt_local_install_root_include_path_makefile = $(bin)install_root_include_path.make

endif

not_install_root_include_path_dependencies = { n=0; for p in $?; do m=0; for d in $(install_root_include_path_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
install_root_include_pathdirs :
	@if test ! -d $(bin)install_root_include_path; then $(mkdir) -p $(bin)install_root_include_path; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)install_root_include_path
else
install_root_include_pathdirs : ;
endif

ifdef cmt_install_root_include_path_has_target_tag

ifndef QUICK
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -tag=$(tags) $(install_root_include_path_extratags) build constituent_config -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path
else
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_root_include_path) ] || \
	  [ ! -f $(cmt_final_setup_install_root_include_path) ] || \
	  $(not_install_root_include_path_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -tag=$(tags) $(install_root_include_path_extratags) build constituent_config -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -f=$(bin)install_root_include_path.in -tag=$(tags) $(install_root_include_path_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path
else
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) $(cmt_build_library_linksstamp) $(bin)install_root_include_path.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_root_include_path) ] || \
	  [ ! -f $(cmt_final_setup_install_root_include_path) ] || \
	  $(not_install_root_include_path_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -f=$(bin)install_root_include_path.in -tag=$(tags) $(install_root_include_path_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(install_root_include_path_extratags) build constituent_makefile -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path

install_root_include_path :: $(install_root_include_path_dependencies) $(cmt_local_install_root_include_path_makefile) dirs install_root_include_pathdirs
	$(echo) "(constituents.make) Starting install_root_include_path"
	@if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_path; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_path
	$(echo) "(constituents.make) install_root_include_path done"

clean :: install_root_include_pathclean ;

install_root_include_pathclean :: $(install_root_include_pathclean_dependencies) ##$(cmt_local_install_root_include_path_makefile)
	$(echo) "(constituents.make) Starting install_root_include_pathclean"
	@-if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_pathclean; \
	fi
	$(echo) "(constituents.make) install_root_include_pathclean done"
#	@-$(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_pathclean

##	  /bin/rm -f $(cmt_local_install_root_include_path_makefile) $(bin)install_root_include_path_dependencies.make

install :: install_root_include_pathinstall ;

install_root_include_pathinstall :: $(install_root_include_path_dependencies) $(cmt_local_install_root_include_path_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_install_root_include_path_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : install_root_include_pathuninstall

$(foreach d,$(install_root_include_path_dependencies),$(eval $(d)uninstall_dependencies += install_root_include_pathuninstall))

install_root_include_pathuninstall : $(install_root_include_pathuninstall_dependencies) ##$(cmt_local_install_root_include_path_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_root_include_path_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: install_root_include_pathuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ install_root_include_path"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ install_root_include_path done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_install_includes_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_install_includes_has_target_tag

cmt_local_tagfile_install_includes = $(bin)$(InDetPhysValMonitoring_tag)_install_includes.make
cmt_final_setup_install_includes = $(bin)setup_install_includes.make
cmt_local_install_includes_makefile = $(bin)install_includes.make

install_includes_extratags = -tag_add=target_install_includes

else

cmt_local_tagfile_install_includes = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_install_includes = $(bin)setup.make
cmt_local_install_includes_makefile = $(bin)install_includes.make

endif

not_install_includes_dependencies = { n=0; for p in $?; do m=0; for d in $(install_includes_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
install_includesdirs :
	@if test ! -d $(bin)install_includes; then $(mkdir) -p $(bin)install_includes; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)install_includes
else
install_includesdirs : ;
endif

ifdef cmt_install_includes_has_target_tag

ifndef QUICK
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -tag=$(tags) $(install_includes_extratags) build constituent_config -out=$(cmt_local_install_includes_makefile) install_includes
else
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_includes) ] || \
	  [ ! -f $(cmt_final_setup_install_includes) ] || \
	  $(not_install_includes_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -tag=$(tags) $(install_includes_extratags) build constituent_config -out=$(cmt_local_install_includes_makefile) install_includes; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -f=$(bin)install_includes.in -tag=$(tags) $(install_includes_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_includes_makefile) install_includes
else
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) $(cmt_build_library_linksstamp) $(bin)install_includes.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_includes) ] || \
	  [ ! -f $(cmt_final_setup_install_includes) ] || \
	  $(not_install_includes_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -f=$(bin)install_includes.in -tag=$(tags) $(install_includes_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_includes_makefile) install_includes; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(install_includes_extratags) build constituent_makefile -out=$(cmt_local_install_includes_makefile) install_includes

install_includes :: $(install_includes_dependencies) $(cmt_local_install_includes_makefile) dirs install_includesdirs
	$(echo) "(constituents.make) Starting install_includes"
	@if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) install_includes; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_includes_makefile) install_includes
	$(echo) "(constituents.make) install_includes done"

clean :: install_includesclean ;

install_includesclean :: $(install_includesclean_dependencies) ##$(cmt_local_install_includes_makefile)
	$(echo) "(constituents.make) Starting install_includesclean"
	@-if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) install_includesclean; \
	fi
	$(echo) "(constituents.make) install_includesclean done"
#	@-$(MAKE) -f $(cmt_local_install_includes_makefile) install_includesclean

##	  /bin/rm -f $(cmt_local_install_includes_makefile) $(bin)install_includes_dependencies.make

install :: install_includesinstall ;

install_includesinstall :: $(install_includes_dependencies) $(cmt_local_install_includes_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_install_includes_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : install_includesuninstall

$(foreach d,$(install_includes_dependencies),$(eval $(d)uninstall_dependencies += install_includesuninstall))

install_includesuninstall : $(install_includesuninstall_dependencies) ##$(cmt_local_install_includes_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_includes_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: install_includesuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ install_includes"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ install_includes done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_myctest_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_myctest_has_target_tag

cmt_local_tagfile_myctest = $(bin)$(InDetPhysValMonitoring_tag)_myctest.make
cmt_final_setup_myctest = $(bin)setup_myctest.make
cmt_local_myctest_makefile = $(bin)myctest.make

myctest_extratags = -tag_add=target_myctest

else

cmt_local_tagfile_myctest = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_myctest = $(bin)setup.make
cmt_local_myctest_makefile = $(bin)myctest.make

endif

not_myctest_dependencies = { n=0; for p in $?; do m=0; for d in $(myctest_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
myctestdirs :
	@if test ! -d $(bin)myctest; then $(mkdir) -p $(bin)myctest; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)myctest
else
myctestdirs : ;
endif

ifdef cmt_myctest_has_target_tag

ifndef QUICK
$(cmt_local_myctest_makefile) : $(myctest_dependencies) build_library_links
	$(echo) "(constituents.make) Building myctest.make"; \
	  $(cmtexe) -tag=$(tags) $(myctest_extratags) build constituent_config -out=$(cmt_local_myctest_makefile) myctest
else
$(cmt_local_myctest_makefile) : $(myctest_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_myctest) ] || \
	  [ ! -f $(cmt_final_setup_myctest) ] || \
	  $(not_myctest_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building myctest.make"; \
	  $(cmtexe) -tag=$(tags) $(myctest_extratags) build constituent_config -out=$(cmt_local_myctest_makefile) myctest; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_myctest_makefile) : $(myctest_dependencies) build_library_links
	$(echo) "(constituents.make) Building myctest.make"; \
	  $(cmtexe) -f=$(bin)myctest.in -tag=$(tags) $(myctest_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_myctest_makefile) myctest
else
$(cmt_local_myctest_makefile) : $(myctest_dependencies) $(cmt_build_library_linksstamp) $(bin)myctest.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_myctest) ] || \
	  [ ! -f $(cmt_final_setup_myctest) ] || \
	  $(not_myctest_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building myctest.make"; \
	  $(cmtexe) -f=$(bin)myctest.in -tag=$(tags) $(myctest_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_myctest_makefile) myctest; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(myctest_extratags) build constituent_makefile -out=$(cmt_local_myctest_makefile) myctest

myctest :: $(myctest_dependencies) $(cmt_local_myctest_makefile) dirs myctestdirs
	$(echo) "(constituents.make) Starting myctest"
	@if test -f $(cmt_local_myctest_makefile); then \
	  $(MAKE) -f $(cmt_local_myctest_makefile) myctest; \
	  fi
#	@$(MAKE) -f $(cmt_local_myctest_makefile) myctest
	$(echo) "(constituents.make) myctest done"

clean :: myctestclean ;

myctestclean :: $(myctestclean_dependencies) ##$(cmt_local_myctest_makefile)
	$(echo) "(constituents.make) Starting myctestclean"
	@-if test -f $(cmt_local_myctest_makefile); then \
	  $(MAKE) -f $(cmt_local_myctest_makefile) myctestclean; \
	fi
	$(echo) "(constituents.make) myctestclean done"
#	@-$(MAKE) -f $(cmt_local_myctest_makefile) myctestclean

##	  /bin/rm -f $(cmt_local_myctest_makefile) $(bin)myctest_dependencies.make

install :: myctestinstall ;

myctestinstall :: $(myctest_dependencies) $(cmt_local_myctest_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_myctest_makefile); then \
	  $(MAKE) -f $(cmt_local_myctest_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_myctest_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : myctestuninstall

$(foreach d,$(myctest_dependencies),$(eval $(d)uninstall_dependencies += myctestuninstall))

myctestuninstall : $(myctestuninstall_dependencies) ##$(cmt_local_myctest_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_myctest_makefile); then \
	  $(MAKE) -f $(cmt_local_myctest_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_myctest_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: myctestuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ myctest"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ myctest done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_make_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_make_has_target_tag

cmt_local_tagfile_make = $(bin)$(InDetPhysValMonitoring_tag)_make.make
cmt_final_setup_make = $(bin)setup_make.make
cmt_local_make_makefile = $(bin)make.make

make_extratags = -tag_add=target_make

else

cmt_local_tagfile_make = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_make = $(bin)setup.make
cmt_local_make_makefile = $(bin)make.make

endif

not_make_dependencies = { n=0; for p in $?; do m=0; for d in $(make_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
makedirs :
	@if test ! -d $(bin)make; then $(mkdir) -p $(bin)make; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)make
else
makedirs : ;
endif

ifdef cmt_make_has_target_tag

ifndef QUICK
$(cmt_local_make_makefile) : $(make_dependencies) build_library_links
	$(echo) "(constituents.make) Building make.make"; \
	  $(cmtexe) -tag=$(tags) $(make_extratags) build constituent_config -out=$(cmt_local_make_makefile) make
else
$(cmt_local_make_makefile) : $(make_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_make) ] || \
	  [ ! -f $(cmt_final_setup_make) ] || \
	  $(not_make_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building make.make"; \
	  $(cmtexe) -tag=$(tags) $(make_extratags) build constituent_config -out=$(cmt_local_make_makefile) make; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_make_makefile) : $(make_dependencies) build_library_links
	$(echo) "(constituents.make) Building make.make"; \
	  $(cmtexe) -f=$(bin)make.in -tag=$(tags) $(make_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_make_makefile) make
else
$(cmt_local_make_makefile) : $(make_dependencies) $(cmt_build_library_linksstamp) $(bin)make.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_make) ] || \
	  [ ! -f $(cmt_final_setup_make) ] || \
	  $(not_make_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building make.make"; \
	  $(cmtexe) -f=$(bin)make.in -tag=$(tags) $(make_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_make_makefile) make; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(make_extratags) build constituent_makefile -out=$(cmt_local_make_makefile) make

make :: $(make_dependencies) $(cmt_local_make_makefile) dirs makedirs
	$(echo) "(constituents.make) Starting make"
	@if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) make; \
	  fi
#	@$(MAKE) -f $(cmt_local_make_makefile) make
	$(echo) "(constituents.make) make done"

clean :: makeclean ;

makeclean :: $(makeclean_dependencies) ##$(cmt_local_make_makefile)
	$(echo) "(constituents.make) Starting makeclean"
	@-if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) makeclean; \
	fi
	$(echo) "(constituents.make) makeclean done"
#	@-$(MAKE) -f $(cmt_local_make_makefile) makeclean

##	  /bin/rm -f $(cmt_local_make_makefile) $(bin)make_dependencies.make

install :: makeinstall ;

makeinstall :: $(make_dependencies) $(cmt_local_make_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_make_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : makeuninstall

$(foreach d,$(make_dependencies),$(eval $(d)uninstall_dependencies += makeuninstall))

makeuninstall : $(makeuninstall_dependencies) ##$(cmt_local_make_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_make_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: makeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ make"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ make done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_CompilePython_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_CompilePython_has_target_tag

cmt_local_tagfile_CompilePython = $(bin)$(InDetPhysValMonitoring_tag)_CompilePython.make
cmt_final_setup_CompilePython = $(bin)setup_CompilePython.make
cmt_local_CompilePython_makefile = $(bin)CompilePython.make

CompilePython_extratags = -tag_add=target_CompilePython

else

cmt_local_tagfile_CompilePython = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_CompilePython = $(bin)setup.make
cmt_local_CompilePython_makefile = $(bin)CompilePython.make

endif

not_CompilePython_dependencies = { n=0; for p in $?; do m=0; for d in $(CompilePython_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
CompilePythondirs :
	@if test ! -d $(bin)CompilePython; then $(mkdir) -p $(bin)CompilePython; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)CompilePython
else
CompilePythondirs : ;
endif

ifdef cmt_CompilePython_has_target_tag

ifndef QUICK
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) build_library_links
	$(echo) "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -tag=$(tags) $(CompilePython_extratags) build constituent_config -out=$(cmt_local_CompilePython_makefile) CompilePython
else
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_CompilePython) ] || \
	  [ ! -f $(cmt_final_setup_CompilePython) ] || \
	  $(not_CompilePython_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -tag=$(tags) $(CompilePython_extratags) build constituent_config -out=$(cmt_local_CompilePython_makefile) CompilePython; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) build_library_links
	$(echo) "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -f=$(bin)CompilePython.in -tag=$(tags) $(CompilePython_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_CompilePython_makefile) CompilePython
else
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) $(cmt_build_library_linksstamp) $(bin)CompilePython.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_CompilePython) ] || \
	  [ ! -f $(cmt_final_setup_CompilePython) ] || \
	  $(not_CompilePython_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -f=$(bin)CompilePython.in -tag=$(tags) $(CompilePython_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_CompilePython_makefile) CompilePython; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(CompilePython_extratags) build constituent_makefile -out=$(cmt_local_CompilePython_makefile) CompilePython

CompilePython :: $(CompilePython_dependencies) $(cmt_local_CompilePython_makefile) dirs CompilePythondirs
	$(echo) "(constituents.make) Starting CompilePython"
	@if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePython; \
	  fi
#	@$(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePython
	$(echo) "(constituents.make) CompilePython done"

clean :: CompilePythonclean ;

CompilePythonclean :: $(CompilePythonclean_dependencies) ##$(cmt_local_CompilePython_makefile)
	$(echo) "(constituents.make) Starting CompilePythonclean"
	@-if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePythonclean; \
	fi
	$(echo) "(constituents.make) CompilePythonclean done"
#	@-$(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePythonclean

##	  /bin/rm -f $(cmt_local_CompilePython_makefile) $(bin)CompilePython_dependencies.make

install :: CompilePythoninstall ;

CompilePythoninstall :: $(CompilePython_dependencies) $(cmt_local_CompilePython_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_CompilePython_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : CompilePythonuninstall

$(foreach d,$(CompilePython_dependencies),$(eval $(d)uninstall_dependencies += CompilePythonuninstall))

CompilePythonuninstall : $(CompilePythonuninstall_dependencies) ##$(cmt_local_CompilePython_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_CompilePython_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: CompilePythonuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ CompilePython"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ CompilePython done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_qmtest_run_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_qmtest_run_has_target_tag

cmt_local_tagfile_qmtest_run = $(bin)$(InDetPhysValMonitoring_tag)_qmtest_run.make
cmt_final_setup_qmtest_run = $(bin)setup_qmtest_run.make
cmt_local_qmtest_run_makefile = $(bin)qmtest_run.make

qmtest_run_extratags = -tag_add=target_qmtest_run

else

cmt_local_tagfile_qmtest_run = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_qmtest_run = $(bin)setup.make
cmt_local_qmtest_run_makefile = $(bin)qmtest_run.make

endif

not_qmtest_run_dependencies = { n=0; for p in $?; do m=0; for d in $(qmtest_run_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
qmtest_rundirs :
	@if test ! -d $(bin)qmtest_run; then $(mkdir) -p $(bin)qmtest_run; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)qmtest_run
else
qmtest_rundirs : ;
endif

ifdef cmt_qmtest_run_has_target_tag

ifndef QUICK
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_run_extratags) build constituent_config -out=$(cmt_local_qmtest_run_makefile) qmtest_run
else
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_run) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_run) ] || \
	  $(not_qmtest_run_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_run_extratags) build constituent_config -out=$(cmt_local_qmtest_run_makefile) qmtest_run; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -f=$(bin)qmtest_run.in -tag=$(tags) $(qmtest_run_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_run_makefile) qmtest_run
else
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) $(cmt_build_library_linksstamp) $(bin)qmtest_run.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_run) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_run) ] || \
	  $(not_qmtest_run_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -f=$(bin)qmtest_run.in -tag=$(tags) $(qmtest_run_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_run_makefile) qmtest_run; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(qmtest_run_extratags) build constituent_makefile -out=$(cmt_local_qmtest_run_makefile) qmtest_run

qmtest_run :: $(qmtest_run_dependencies) $(cmt_local_qmtest_run_makefile) dirs qmtest_rundirs
	$(echo) "(constituents.make) Starting qmtest_run"
	@if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_run; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_run
	$(echo) "(constituents.make) qmtest_run done"

clean :: qmtest_runclean ;

qmtest_runclean :: $(qmtest_runclean_dependencies) ##$(cmt_local_qmtest_run_makefile)
	$(echo) "(constituents.make) Starting qmtest_runclean"
	@-if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_runclean; \
	fi
	$(echo) "(constituents.make) qmtest_runclean done"
#	@-$(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_runclean

##	  /bin/rm -f $(cmt_local_qmtest_run_makefile) $(bin)qmtest_run_dependencies.make

install :: qmtest_runinstall ;

qmtest_runinstall :: $(qmtest_run_dependencies) $(cmt_local_qmtest_run_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_qmtest_run_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : qmtest_rununinstall

$(foreach d,$(qmtest_run_dependencies),$(eval $(d)uninstall_dependencies += qmtest_rununinstall))

qmtest_rununinstall : $(qmtest_rununinstall_dependencies) ##$(cmt_local_qmtest_run_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_run_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: qmtest_rununinstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ qmtest_run"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ qmtest_run done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_qmtest_summarize_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_qmtest_summarize_has_target_tag

cmt_local_tagfile_qmtest_summarize = $(bin)$(InDetPhysValMonitoring_tag)_qmtest_summarize.make
cmt_final_setup_qmtest_summarize = $(bin)setup_qmtest_summarize.make
cmt_local_qmtest_summarize_makefile = $(bin)qmtest_summarize.make

qmtest_summarize_extratags = -tag_add=target_qmtest_summarize

else

cmt_local_tagfile_qmtest_summarize = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_qmtest_summarize = $(bin)setup.make
cmt_local_qmtest_summarize_makefile = $(bin)qmtest_summarize.make

endif

not_qmtest_summarize_dependencies = { n=0; for p in $?; do m=0; for d in $(qmtest_summarize_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
qmtest_summarizedirs :
	@if test ! -d $(bin)qmtest_summarize; then $(mkdir) -p $(bin)qmtest_summarize; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)qmtest_summarize
else
qmtest_summarizedirs : ;
endif

ifdef cmt_qmtest_summarize_has_target_tag

ifndef QUICK
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_summarize_extratags) build constituent_config -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize
else
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_summarize) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_summarize) ] || \
	  $(not_qmtest_summarize_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_summarize_extratags) build constituent_config -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -f=$(bin)qmtest_summarize.in -tag=$(tags) $(qmtest_summarize_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize
else
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) $(cmt_build_library_linksstamp) $(bin)qmtest_summarize.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_summarize) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_summarize) ] || \
	  $(not_qmtest_summarize_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -f=$(bin)qmtest_summarize.in -tag=$(tags) $(qmtest_summarize_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(qmtest_summarize_extratags) build constituent_makefile -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize

qmtest_summarize :: $(qmtest_summarize_dependencies) $(cmt_local_qmtest_summarize_makefile) dirs qmtest_summarizedirs
	$(echo) "(constituents.make) Starting qmtest_summarize"
	@if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarize; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarize
	$(echo) "(constituents.make) qmtest_summarize done"

clean :: qmtest_summarizeclean ;

qmtest_summarizeclean :: $(qmtest_summarizeclean_dependencies) ##$(cmt_local_qmtest_summarize_makefile)
	$(echo) "(constituents.make) Starting qmtest_summarizeclean"
	@-if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarizeclean; \
	fi
	$(echo) "(constituents.make) qmtest_summarizeclean done"
#	@-$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarizeclean

##	  /bin/rm -f $(cmt_local_qmtest_summarize_makefile) $(bin)qmtest_summarize_dependencies.make

install :: qmtest_summarizeinstall ;

qmtest_summarizeinstall :: $(qmtest_summarize_dependencies) $(cmt_local_qmtest_summarize_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : qmtest_summarizeuninstall

$(foreach d,$(qmtest_summarize_dependencies),$(eval $(d)uninstall_dependencies += qmtest_summarizeuninstall))

qmtest_summarizeuninstall : $(qmtest_summarizeuninstall_dependencies) ##$(cmt_local_qmtest_summarize_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: qmtest_summarizeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ qmtest_summarize"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ qmtest_summarize done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TestPackage_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TestPackage_has_target_tag

cmt_local_tagfile_TestPackage = $(bin)$(InDetPhysValMonitoring_tag)_TestPackage.make
cmt_final_setup_TestPackage = $(bin)setup_TestPackage.make
cmt_local_TestPackage_makefile = $(bin)TestPackage.make

TestPackage_extratags = -tag_add=target_TestPackage

else

cmt_local_tagfile_TestPackage = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_TestPackage = $(bin)setup.make
cmt_local_TestPackage_makefile = $(bin)TestPackage.make

endif

not_TestPackage_dependencies = { n=0; for p in $?; do m=0; for d in $(TestPackage_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TestPackagedirs :
	@if test ! -d $(bin)TestPackage; then $(mkdir) -p $(bin)TestPackage; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TestPackage
else
TestPackagedirs : ;
endif

ifdef cmt_TestPackage_has_target_tag

ifndef QUICK
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -tag=$(tags) $(TestPackage_extratags) build constituent_config -out=$(cmt_local_TestPackage_makefile) TestPackage
else
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestPackage) ] || \
	  [ ! -f $(cmt_final_setup_TestPackage) ] || \
	  $(not_TestPackage_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -tag=$(tags) $(TestPackage_extratags) build constituent_config -out=$(cmt_local_TestPackage_makefile) TestPackage; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -f=$(bin)TestPackage.in -tag=$(tags) $(TestPackage_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestPackage_makefile) TestPackage
else
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) $(cmt_build_library_linksstamp) $(bin)TestPackage.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestPackage) ] || \
	  [ ! -f $(cmt_final_setup_TestPackage) ] || \
	  $(not_TestPackage_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -f=$(bin)TestPackage.in -tag=$(tags) $(TestPackage_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestPackage_makefile) TestPackage; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TestPackage_extratags) build constituent_makefile -out=$(cmt_local_TestPackage_makefile) TestPackage

TestPackage :: $(TestPackage_dependencies) $(cmt_local_TestPackage_makefile) dirs TestPackagedirs
	$(echo) "(constituents.make) Starting TestPackage"
	@if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackage; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackage
	$(echo) "(constituents.make) TestPackage done"

clean :: TestPackageclean ;

TestPackageclean :: $(TestPackageclean_dependencies) ##$(cmt_local_TestPackage_makefile)
	$(echo) "(constituents.make) Starting TestPackageclean"
	@-if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackageclean; \
	fi
	$(echo) "(constituents.make) TestPackageclean done"
#	@-$(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackageclean

##	  /bin/rm -f $(cmt_local_TestPackage_makefile) $(bin)TestPackage_dependencies.make

install :: TestPackageinstall ;

TestPackageinstall :: $(TestPackage_dependencies) $(cmt_local_TestPackage_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TestPackage_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TestPackageuninstall

$(foreach d,$(TestPackage_dependencies),$(eval $(d)uninstall_dependencies += TestPackageuninstall))

TestPackageuninstall : $(TestPackageuninstall_dependencies) ##$(cmt_local_TestPackage_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestPackage_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TestPackageuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TestPackage"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TestPackage done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TestProject_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TestProject_has_target_tag

cmt_local_tagfile_TestProject = $(bin)$(InDetPhysValMonitoring_tag)_TestProject.make
cmt_final_setup_TestProject = $(bin)setup_TestProject.make
cmt_local_TestProject_makefile = $(bin)TestProject.make

TestProject_extratags = -tag_add=target_TestProject

else

cmt_local_tagfile_TestProject = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_TestProject = $(bin)setup.make
cmt_local_TestProject_makefile = $(bin)TestProject.make

endif

not_TestProject_dependencies = { n=0; for p in $?; do m=0; for d in $(TestProject_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TestProjectdirs :
	@if test ! -d $(bin)TestProject; then $(mkdir) -p $(bin)TestProject; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TestProject
else
TestProjectdirs : ;
endif

ifdef cmt_TestProject_has_target_tag

ifndef QUICK
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -tag=$(tags) $(TestProject_extratags) build constituent_config -out=$(cmt_local_TestProject_makefile) TestProject
else
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestProject) ] || \
	  [ ! -f $(cmt_final_setup_TestProject) ] || \
	  $(not_TestProject_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -tag=$(tags) $(TestProject_extratags) build constituent_config -out=$(cmt_local_TestProject_makefile) TestProject; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -f=$(bin)TestProject.in -tag=$(tags) $(TestProject_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestProject_makefile) TestProject
else
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) $(cmt_build_library_linksstamp) $(bin)TestProject.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestProject) ] || \
	  [ ! -f $(cmt_final_setup_TestProject) ] || \
	  $(not_TestProject_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -f=$(bin)TestProject.in -tag=$(tags) $(TestProject_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestProject_makefile) TestProject; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TestProject_extratags) build constituent_makefile -out=$(cmt_local_TestProject_makefile) TestProject

TestProject :: $(TestProject_dependencies) $(cmt_local_TestProject_makefile) dirs TestProjectdirs
	$(echo) "(constituents.make) Starting TestProject"
	@if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) TestProject; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestProject_makefile) TestProject
	$(echo) "(constituents.make) TestProject done"

clean :: TestProjectclean ;

TestProjectclean :: $(TestProjectclean_dependencies) ##$(cmt_local_TestProject_makefile)
	$(echo) "(constituents.make) Starting TestProjectclean"
	@-if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) TestProjectclean; \
	fi
	$(echo) "(constituents.make) TestProjectclean done"
#	@-$(MAKE) -f $(cmt_local_TestProject_makefile) TestProjectclean

##	  /bin/rm -f $(cmt_local_TestProject_makefile) $(bin)TestProject_dependencies.make

install :: TestProjectinstall ;

TestProjectinstall :: $(TestProject_dependencies) $(cmt_local_TestProject_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TestProject_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TestProjectuninstall

$(foreach d,$(TestProject_dependencies),$(eval $(d)uninstall_dependencies += TestProjectuninstall))

TestProjectuninstall : $(TestProjectuninstall_dependencies) ##$(cmt_local_TestProject_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestProject_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TestProjectuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TestProject"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TestProject done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_new_rootsys_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_new_rootsys_has_target_tag

cmt_local_tagfile_new_rootsys = $(bin)$(InDetPhysValMonitoring_tag)_new_rootsys.make
cmt_final_setup_new_rootsys = $(bin)setup_new_rootsys.make
cmt_local_new_rootsys_makefile = $(bin)new_rootsys.make

new_rootsys_extratags = -tag_add=target_new_rootsys

else

cmt_local_tagfile_new_rootsys = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_new_rootsys = $(bin)setup.make
cmt_local_new_rootsys_makefile = $(bin)new_rootsys.make

endif

not_new_rootsys_dependencies = { n=0; for p in $?; do m=0; for d in $(new_rootsys_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
new_rootsysdirs :
	@if test ! -d $(bin)new_rootsys; then $(mkdir) -p $(bin)new_rootsys; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)new_rootsys
else
new_rootsysdirs : ;
endif

ifdef cmt_new_rootsys_has_target_tag

ifndef QUICK
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) build_library_links
	$(echo) "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -tag=$(tags) $(new_rootsys_extratags) build constituent_config -out=$(cmt_local_new_rootsys_makefile) new_rootsys
else
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_new_rootsys) ] || \
	  [ ! -f $(cmt_final_setup_new_rootsys) ] || \
	  $(not_new_rootsys_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -tag=$(tags) $(new_rootsys_extratags) build constituent_config -out=$(cmt_local_new_rootsys_makefile) new_rootsys; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) build_library_links
	$(echo) "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -f=$(bin)new_rootsys.in -tag=$(tags) $(new_rootsys_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_new_rootsys_makefile) new_rootsys
else
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) $(cmt_build_library_linksstamp) $(bin)new_rootsys.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_new_rootsys) ] || \
	  [ ! -f $(cmt_final_setup_new_rootsys) ] || \
	  $(not_new_rootsys_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -f=$(bin)new_rootsys.in -tag=$(tags) $(new_rootsys_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_new_rootsys_makefile) new_rootsys; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(new_rootsys_extratags) build constituent_makefile -out=$(cmt_local_new_rootsys_makefile) new_rootsys

new_rootsys :: $(new_rootsys_dependencies) $(cmt_local_new_rootsys_makefile) dirs new_rootsysdirs
	$(echo) "(constituents.make) Starting new_rootsys"
	@if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsys; \
	  fi
#	@$(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsys
	$(echo) "(constituents.make) new_rootsys done"

clean :: new_rootsysclean ;

new_rootsysclean :: $(new_rootsysclean_dependencies) ##$(cmt_local_new_rootsys_makefile)
	$(echo) "(constituents.make) Starting new_rootsysclean"
	@-if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsysclean; \
	fi
	$(echo) "(constituents.make) new_rootsysclean done"
#	@-$(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsysclean

##	  /bin/rm -f $(cmt_local_new_rootsys_makefile) $(bin)new_rootsys_dependencies.make

install :: new_rootsysinstall ;

new_rootsysinstall :: $(new_rootsys_dependencies) $(cmt_local_new_rootsys_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_new_rootsys_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : new_rootsysuninstall

$(foreach d,$(new_rootsys_dependencies),$(eval $(d)uninstall_dependencies += new_rootsysuninstall))

new_rootsysuninstall : $(new_rootsysuninstall_dependencies) ##$(cmt_local_new_rootsys_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_new_rootsys_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: new_rootsysuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ new_rootsys"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ new_rootsys done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_doxygen_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_doxygen_has_target_tag

cmt_local_tagfile_doxygen = $(bin)$(InDetPhysValMonitoring_tag)_doxygen.make
cmt_final_setup_doxygen = $(bin)setup_doxygen.make
cmt_local_doxygen_makefile = $(bin)doxygen.make

doxygen_extratags = -tag_add=target_doxygen

else

cmt_local_tagfile_doxygen = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_doxygen = $(bin)setup.make
cmt_local_doxygen_makefile = $(bin)doxygen.make

endif

not_doxygen_dependencies = { n=0; for p in $?; do m=0; for d in $(doxygen_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
doxygendirs :
	@if test ! -d $(bin)doxygen; then $(mkdir) -p $(bin)doxygen; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)doxygen
else
doxygendirs : ;
endif

ifdef cmt_doxygen_has_target_tag

ifndef QUICK
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) build_library_links
	$(echo) "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -tag=$(tags) $(doxygen_extratags) build constituent_config -out=$(cmt_local_doxygen_makefile) doxygen
else
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_doxygen) ] || \
	  [ ! -f $(cmt_final_setup_doxygen) ] || \
	  $(not_doxygen_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -tag=$(tags) $(doxygen_extratags) build constituent_config -out=$(cmt_local_doxygen_makefile) doxygen; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) build_library_links
	$(echo) "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -f=$(bin)doxygen.in -tag=$(tags) $(doxygen_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_doxygen_makefile) doxygen
else
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) $(cmt_build_library_linksstamp) $(bin)doxygen.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_doxygen) ] || \
	  [ ! -f $(cmt_final_setup_doxygen) ] || \
	  $(not_doxygen_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -f=$(bin)doxygen.in -tag=$(tags) $(doxygen_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_doxygen_makefile) doxygen; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(doxygen_extratags) build constituent_makefile -out=$(cmt_local_doxygen_makefile) doxygen

doxygen :: $(doxygen_dependencies) $(cmt_local_doxygen_makefile) dirs doxygendirs
	$(echo) "(constituents.make) Starting doxygen"
	@if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) doxygen; \
	  fi
#	@$(MAKE) -f $(cmt_local_doxygen_makefile) doxygen
	$(echo) "(constituents.make) doxygen done"

clean :: doxygenclean ;

doxygenclean :: $(doxygenclean_dependencies) ##$(cmt_local_doxygen_makefile)
	$(echo) "(constituents.make) Starting doxygenclean"
	@-if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) doxygenclean; \
	fi
	$(echo) "(constituents.make) doxygenclean done"
#	@-$(MAKE) -f $(cmt_local_doxygen_makefile) doxygenclean

##	  /bin/rm -f $(cmt_local_doxygen_makefile) $(bin)doxygen_dependencies.make

install :: doxygeninstall ;

doxygeninstall :: $(doxygen_dependencies) $(cmt_local_doxygen_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_doxygen_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : doxygenuninstall

$(foreach d,$(doxygen_dependencies),$(eval $(d)uninstall_dependencies += doxygenuninstall))

doxygenuninstall : $(doxygenuninstall_dependencies) ##$(cmt_local_doxygen_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_doxygen_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: doxygenuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ doxygen"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ doxygen done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_install_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_install_has_target_tag

cmt_local_tagfile_post_install = $(bin)$(InDetPhysValMonitoring_tag)_post_install.make
cmt_final_setup_post_install = $(bin)setup_post_install.make
cmt_local_post_install_makefile = $(bin)post_install.make

post_install_extratags = -tag_add=target_post_install

else

cmt_local_tagfile_post_install = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_post_install = $(bin)setup.make
cmt_local_post_install_makefile = $(bin)post_install.make

endif

not_post_install_dependencies = { n=0; for p in $?; do m=0; for d in $(post_install_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_installdirs :
	@if test ! -d $(bin)post_install; then $(mkdir) -p $(bin)post_install; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_install
else
post_installdirs : ;
endif

ifdef cmt_post_install_has_target_tag

ifndef QUICK
$(cmt_local_post_install_makefile) : $(post_install_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -tag=$(tags) $(post_install_extratags) build constituent_config -out=$(cmt_local_post_install_makefile) post_install
else
$(cmt_local_post_install_makefile) : $(post_install_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_install) ] || \
	  [ ! -f $(cmt_final_setup_post_install) ] || \
	  $(not_post_install_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -tag=$(tags) $(post_install_extratags) build constituent_config -out=$(cmt_local_post_install_makefile) post_install; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_install_makefile) : $(post_install_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -f=$(bin)post_install.in -tag=$(tags) $(post_install_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_install_makefile) post_install
else
$(cmt_local_post_install_makefile) : $(post_install_dependencies) $(cmt_build_library_linksstamp) $(bin)post_install.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_install) ] || \
	  [ ! -f $(cmt_final_setup_post_install) ] || \
	  $(not_post_install_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -f=$(bin)post_install.in -tag=$(tags) $(post_install_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_install_makefile) post_install; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_install_extratags) build constituent_makefile -out=$(cmt_local_post_install_makefile) post_install

post_install :: $(post_install_dependencies) $(cmt_local_post_install_makefile) dirs post_installdirs
	$(echo) "(constituents.make) Starting post_install"
	@if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) post_install; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_install_makefile) post_install
	$(echo) "(constituents.make) post_install done"

clean :: post_installclean ;

post_installclean :: $(post_installclean_dependencies) ##$(cmt_local_post_install_makefile)
	$(echo) "(constituents.make) Starting post_installclean"
	@-if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) post_installclean; \
	fi
	$(echo) "(constituents.make) post_installclean done"
#	@-$(MAKE) -f $(cmt_local_post_install_makefile) post_installclean

##	  /bin/rm -f $(cmt_local_post_install_makefile) $(bin)post_install_dependencies.make

install :: post_installinstall ;

post_installinstall :: $(post_install_dependencies) $(cmt_local_post_install_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_install_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_installuninstall

$(foreach d,$(post_install_dependencies),$(eval $(d)uninstall_dependencies += post_installuninstall))

post_installuninstall : $(post_installuninstall_dependencies) ##$(cmt_local_post_install_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_install_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_installuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_install"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_install done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_merge_rootmap_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_merge_rootmap_has_target_tag

cmt_local_tagfile_post_merge_rootmap = $(bin)$(InDetPhysValMonitoring_tag)_post_merge_rootmap.make
cmt_final_setup_post_merge_rootmap = $(bin)setup_post_merge_rootmap.make
cmt_local_post_merge_rootmap_makefile = $(bin)post_merge_rootmap.make

post_merge_rootmap_extratags = -tag_add=target_post_merge_rootmap

else

cmt_local_tagfile_post_merge_rootmap = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_post_merge_rootmap = $(bin)setup.make
cmt_local_post_merge_rootmap_makefile = $(bin)post_merge_rootmap.make

endif

not_post_merge_rootmap_dependencies = { n=0; for p in $?; do m=0; for d in $(post_merge_rootmap_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_merge_rootmapdirs :
	@if test ! -d $(bin)post_merge_rootmap; then $(mkdir) -p $(bin)post_merge_rootmap; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_merge_rootmap
else
post_merge_rootmapdirs : ;
endif

ifdef cmt_post_merge_rootmap_has_target_tag

ifndef QUICK
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_config -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap
else
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_rootmap) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_rootmap) ] || \
	  $(not_post_merge_rootmap_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_config -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -f=$(bin)post_merge_rootmap.in -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap
else
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) $(cmt_build_library_linksstamp) $(bin)post_merge_rootmap.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_rootmap) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_rootmap) ] || \
	  $(not_post_merge_rootmap_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -f=$(bin)post_merge_rootmap.in -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_makefile -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap

post_merge_rootmap :: $(post_merge_rootmap_dependencies) $(cmt_local_post_merge_rootmap_makefile) dirs post_merge_rootmapdirs
	$(echo) "(constituents.make) Starting post_merge_rootmap"
	@if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap
	$(echo) "(constituents.make) post_merge_rootmap done"

clean :: post_merge_rootmapclean ;

post_merge_rootmapclean :: $(post_merge_rootmapclean_dependencies) ##$(cmt_local_post_merge_rootmap_makefile)
	$(echo) "(constituents.make) Starting post_merge_rootmapclean"
	@-if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmapclean; \
	fi
	$(echo) "(constituents.make) post_merge_rootmapclean done"
#	@-$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmapclean

##	  /bin/rm -f $(cmt_local_post_merge_rootmap_makefile) $(bin)post_merge_rootmap_dependencies.make

install :: post_merge_rootmapinstall ;

post_merge_rootmapinstall :: $(post_merge_rootmap_dependencies) $(cmt_local_post_merge_rootmap_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_merge_rootmapuninstall

$(foreach d,$(post_merge_rootmap_dependencies),$(eval $(d)uninstall_dependencies += post_merge_rootmapuninstall))

post_merge_rootmapuninstall : $(post_merge_rootmapuninstall_dependencies) ##$(cmt_local_post_merge_rootmap_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_merge_rootmapuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_merge_rootmap"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_merge_rootmap done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_merge_genconfdb_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_merge_genconfdb_has_target_tag

cmt_local_tagfile_post_merge_genconfdb = $(bin)$(InDetPhysValMonitoring_tag)_post_merge_genconfdb.make
cmt_final_setup_post_merge_genconfdb = $(bin)setup_post_merge_genconfdb.make
cmt_local_post_merge_genconfdb_makefile = $(bin)post_merge_genconfdb.make

post_merge_genconfdb_extratags = -tag_add=target_post_merge_genconfdb

else

cmt_local_tagfile_post_merge_genconfdb = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_post_merge_genconfdb = $(bin)setup.make
cmt_local_post_merge_genconfdb_makefile = $(bin)post_merge_genconfdb.make

endif

not_post_merge_genconfdb_dependencies = { n=0; for p in $?; do m=0; for d in $(post_merge_genconfdb_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_merge_genconfdbdirs :
	@if test ! -d $(bin)post_merge_genconfdb; then $(mkdir) -p $(bin)post_merge_genconfdb; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_merge_genconfdb
else
post_merge_genconfdbdirs : ;
endif

ifdef cmt_post_merge_genconfdb_has_target_tag

ifndef QUICK
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_config -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb
else
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_genconfdb) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_genconfdb) ] || \
	  $(not_post_merge_genconfdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_config -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -f=$(bin)post_merge_genconfdb.in -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb
else
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) $(cmt_build_library_linksstamp) $(bin)post_merge_genconfdb.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_genconfdb) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_genconfdb) ] || \
	  $(not_post_merge_genconfdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -f=$(bin)post_merge_genconfdb.in -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_makefile -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb

post_merge_genconfdb :: $(post_merge_genconfdb_dependencies) $(cmt_local_post_merge_genconfdb_makefile) dirs post_merge_genconfdbdirs
	$(echo) "(constituents.make) Starting post_merge_genconfdb"
	@if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb
	$(echo) "(constituents.make) post_merge_genconfdb done"

clean :: post_merge_genconfdbclean ;

post_merge_genconfdbclean :: $(post_merge_genconfdbclean_dependencies) ##$(cmt_local_post_merge_genconfdb_makefile)
	$(echo) "(constituents.make) Starting post_merge_genconfdbclean"
	@-if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdbclean; \
	fi
	$(echo) "(constituents.make) post_merge_genconfdbclean done"
#	@-$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdbclean

##	  /bin/rm -f $(cmt_local_post_merge_genconfdb_makefile) $(bin)post_merge_genconfdb_dependencies.make

install :: post_merge_genconfdbinstall ;

post_merge_genconfdbinstall :: $(post_merge_genconfdb_dependencies) $(cmt_local_post_merge_genconfdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_merge_genconfdbuninstall

$(foreach d,$(post_merge_genconfdb_dependencies),$(eval $(d)uninstall_dependencies += post_merge_genconfdbuninstall))

post_merge_genconfdbuninstall : $(post_merge_genconfdbuninstall_dependencies) ##$(cmt_local_post_merge_genconfdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_merge_genconfdbuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_merge_genconfdb"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_merge_genconfdb done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_build_tpcnvdb_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_build_tpcnvdb_has_target_tag

cmt_local_tagfile_post_build_tpcnvdb = $(bin)$(InDetPhysValMonitoring_tag)_post_build_tpcnvdb.make
cmt_final_setup_post_build_tpcnvdb = $(bin)setup_post_build_tpcnvdb.make
cmt_local_post_build_tpcnvdb_makefile = $(bin)post_build_tpcnvdb.make

post_build_tpcnvdb_extratags = -tag_add=target_post_build_tpcnvdb

else

cmt_local_tagfile_post_build_tpcnvdb = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_post_build_tpcnvdb = $(bin)setup.make
cmt_local_post_build_tpcnvdb_makefile = $(bin)post_build_tpcnvdb.make

endif

not_post_build_tpcnvdb_dependencies = { n=0; for p in $?; do m=0; for d in $(post_build_tpcnvdb_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_build_tpcnvdbdirs :
	@if test ! -d $(bin)post_build_tpcnvdb; then $(mkdir) -p $(bin)post_build_tpcnvdb; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_build_tpcnvdb
else
post_build_tpcnvdbdirs : ;
endif

ifdef cmt_post_build_tpcnvdb_has_target_tag

ifndef QUICK
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_config -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb
else
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_build_tpcnvdb) ] || \
	  [ ! -f $(cmt_final_setup_post_build_tpcnvdb) ] || \
	  $(not_post_build_tpcnvdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_config -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -f=$(bin)post_build_tpcnvdb.in -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb
else
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) $(cmt_build_library_linksstamp) $(bin)post_build_tpcnvdb.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_build_tpcnvdb) ] || \
	  [ ! -f $(cmt_final_setup_post_build_tpcnvdb) ] || \
	  $(not_post_build_tpcnvdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -f=$(bin)post_build_tpcnvdb.in -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_makefile -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb

post_build_tpcnvdb :: $(post_build_tpcnvdb_dependencies) $(cmt_local_post_build_tpcnvdb_makefile) dirs post_build_tpcnvdbdirs
	$(echo) "(constituents.make) Starting post_build_tpcnvdb"
	@if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb
	$(echo) "(constituents.make) post_build_tpcnvdb done"

clean :: post_build_tpcnvdbclean ;

post_build_tpcnvdbclean :: $(post_build_tpcnvdbclean_dependencies) ##$(cmt_local_post_build_tpcnvdb_makefile)
	$(echo) "(constituents.make) Starting post_build_tpcnvdbclean"
	@-if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdbclean; \
	fi
	$(echo) "(constituents.make) post_build_tpcnvdbclean done"
#	@-$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdbclean

##	  /bin/rm -f $(cmt_local_post_build_tpcnvdb_makefile) $(bin)post_build_tpcnvdb_dependencies.make

install :: post_build_tpcnvdbinstall ;

post_build_tpcnvdbinstall :: $(post_build_tpcnvdb_dependencies) $(cmt_local_post_build_tpcnvdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_build_tpcnvdbuninstall

$(foreach d,$(post_build_tpcnvdb_dependencies),$(eval $(d)uninstall_dependencies += post_build_tpcnvdbuninstall))

post_build_tpcnvdbuninstall : $(post_build_tpcnvdbuninstall_dependencies) ##$(cmt_local_post_build_tpcnvdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_build_tpcnvdbuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_build_tpcnvdb"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_build_tpcnvdb done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_all_post_constituents_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_all_post_constituents_has_target_tag

cmt_local_tagfile_all_post_constituents = $(bin)$(InDetPhysValMonitoring_tag)_all_post_constituents.make
cmt_final_setup_all_post_constituents = $(bin)setup_all_post_constituents.make
cmt_local_all_post_constituents_makefile = $(bin)all_post_constituents.make

all_post_constituents_extratags = -tag_add=target_all_post_constituents

else

cmt_local_tagfile_all_post_constituents = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_all_post_constituents = $(bin)setup.make
cmt_local_all_post_constituents_makefile = $(bin)all_post_constituents.make

endif

not_all_post_constituents_dependencies = { n=0; for p in $?; do m=0; for d in $(all_post_constituents_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
all_post_constituentsdirs :
	@if test ! -d $(bin)all_post_constituents; then $(mkdir) -p $(bin)all_post_constituents; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)all_post_constituents
else
all_post_constituentsdirs : ;
endif

ifdef cmt_all_post_constituents_has_target_tag

ifndef QUICK
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) build_library_links
	$(echo) "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -tag=$(tags) $(all_post_constituents_extratags) build constituent_config -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents
else
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_all_post_constituents) ] || \
	  [ ! -f $(cmt_final_setup_all_post_constituents) ] || \
	  $(not_all_post_constituents_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -tag=$(tags) $(all_post_constituents_extratags) build constituent_config -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) build_library_links
	$(echo) "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -f=$(bin)all_post_constituents.in -tag=$(tags) $(all_post_constituents_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents
else
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) $(cmt_build_library_linksstamp) $(bin)all_post_constituents.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_all_post_constituents) ] || \
	  [ ! -f $(cmt_final_setup_all_post_constituents) ] || \
	  $(not_all_post_constituents_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -f=$(bin)all_post_constituents.in -tag=$(tags) $(all_post_constituents_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(all_post_constituents_extratags) build constituent_makefile -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents

all_post_constituents :: $(all_post_constituents_dependencies) $(cmt_local_all_post_constituents_makefile) dirs all_post_constituentsdirs
	$(echo) "(constituents.make) Starting all_post_constituents"
	@if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituents; \
	  fi
#	@$(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituents
	$(echo) "(constituents.make) all_post_constituents done"

clean :: all_post_constituentsclean ;

all_post_constituentsclean :: $(all_post_constituentsclean_dependencies) ##$(cmt_local_all_post_constituents_makefile)
	$(echo) "(constituents.make) Starting all_post_constituentsclean"
	@-if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituentsclean; \
	fi
	$(echo) "(constituents.make) all_post_constituentsclean done"
#	@-$(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituentsclean

##	  /bin/rm -f $(cmt_local_all_post_constituents_makefile) $(bin)all_post_constituents_dependencies.make

install :: all_post_constituentsinstall ;

all_post_constituentsinstall :: $(all_post_constituents_dependencies) $(cmt_local_all_post_constituents_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_all_post_constituents_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : all_post_constituentsuninstall

$(foreach d,$(all_post_constituents_dependencies),$(eval $(d)uninstall_dependencies += all_post_constituentsuninstall))

all_post_constituentsuninstall : $(all_post_constituentsuninstall_dependencies) ##$(cmt_local_all_post_constituents_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_all_post_constituents_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: all_post_constituentsuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ all_post_constituents"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ all_post_constituents done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_checkreq_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_checkreq_has_target_tag

cmt_local_tagfile_checkreq = $(bin)$(InDetPhysValMonitoring_tag)_checkreq.make
cmt_final_setup_checkreq = $(bin)setup_checkreq.make
cmt_local_checkreq_makefile = $(bin)checkreq.make

checkreq_extratags = -tag_add=target_checkreq

else

cmt_local_tagfile_checkreq = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_checkreq = $(bin)setup.make
cmt_local_checkreq_makefile = $(bin)checkreq.make

endif

not_checkreq_dependencies = { n=0; for p in $?; do m=0; for d in $(checkreq_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
checkreqdirs :
	@if test ! -d $(bin)checkreq; then $(mkdir) -p $(bin)checkreq; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)checkreq
else
checkreqdirs : ;
endif

ifdef cmt_checkreq_has_target_tag

ifndef QUICK
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) build_library_links
	$(echo) "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -tag=$(tags) $(checkreq_extratags) build constituent_config -out=$(cmt_local_checkreq_makefile) checkreq
else
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_checkreq) ] || \
	  [ ! -f $(cmt_final_setup_checkreq) ] || \
	  $(not_checkreq_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -tag=$(tags) $(checkreq_extratags) build constituent_config -out=$(cmt_local_checkreq_makefile) checkreq; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) build_library_links
	$(echo) "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -f=$(bin)checkreq.in -tag=$(tags) $(checkreq_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_checkreq_makefile) checkreq
else
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) $(cmt_build_library_linksstamp) $(bin)checkreq.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_checkreq) ] || \
	  [ ! -f $(cmt_final_setup_checkreq) ] || \
	  $(not_checkreq_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -f=$(bin)checkreq.in -tag=$(tags) $(checkreq_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_checkreq_makefile) checkreq; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(checkreq_extratags) build constituent_makefile -out=$(cmt_local_checkreq_makefile) checkreq

checkreq :: $(checkreq_dependencies) $(cmt_local_checkreq_makefile) dirs checkreqdirs
	$(echo) "(constituents.make) Starting checkreq"
	@if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) checkreq; \
	  fi
#	@$(MAKE) -f $(cmt_local_checkreq_makefile) checkreq
	$(echo) "(constituents.make) checkreq done"

clean :: checkreqclean ;

checkreqclean :: $(checkreqclean_dependencies) ##$(cmt_local_checkreq_makefile)
	$(echo) "(constituents.make) Starting checkreqclean"
	@-if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) checkreqclean; \
	fi
	$(echo) "(constituents.make) checkreqclean done"
#	@-$(MAKE) -f $(cmt_local_checkreq_makefile) checkreqclean

##	  /bin/rm -f $(cmt_local_checkreq_makefile) $(bin)checkreq_dependencies.make

install :: checkreqinstall ;

checkreqinstall :: $(checkreq_dependencies) $(cmt_local_checkreq_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_checkreq_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : checkrequninstall

$(foreach d,$(checkreq_dependencies),$(eval $(d)uninstall_dependencies += checkrequninstall))

checkrequninstall : $(checkrequninstall_dependencies) ##$(cmt_local_checkreq_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_checkreq_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: checkrequninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ checkreq"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ checkreq done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_check_install_runtime_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_check_install_runtime_has_target_tag

cmt_local_tagfile_check_install_runtime = $(bin)$(InDetPhysValMonitoring_tag)_check_install_runtime.make
cmt_final_setup_check_install_runtime = $(bin)setup_check_install_runtime.make
cmt_local_check_install_runtime_makefile = $(bin)check_install_runtime.make

check_install_runtime_extratags = -tag_add=target_check_install_runtime

else

cmt_local_tagfile_check_install_runtime = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_check_install_runtime = $(bin)setup.make
cmt_local_check_install_runtime_makefile = $(bin)check_install_runtime.make

endif

not_check_install_runtime_dependencies = { n=0; for p in $?; do m=0; for d in $(check_install_runtime_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
check_install_runtimedirs :
	@if test ! -d $(bin)check_install_runtime; then $(mkdir) -p $(bin)check_install_runtime; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)check_install_runtime
else
check_install_runtimedirs : ;
endif

ifdef cmt_check_install_runtime_has_target_tag

ifndef QUICK
$(cmt_local_check_install_runtime_makefile) : $(check_install_runtime_dependencies) build_library_links
	$(echo) "(constituents.make) Building check_install_runtime.make"; \
	  $(cmtexe) -tag=$(tags) $(check_install_runtime_extratags) build constituent_config -out=$(cmt_local_check_install_runtime_makefile) check_install_runtime
else
$(cmt_local_check_install_runtime_makefile) : $(check_install_runtime_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_check_install_runtime) ] || \
	  [ ! -f $(cmt_final_setup_check_install_runtime) ] || \
	  $(not_check_install_runtime_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building check_install_runtime.make"; \
	  $(cmtexe) -tag=$(tags) $(check_install_runtime_extratags) build constituent_config -out=$(cmt_local_check_install_runtime_makefile) check_install_runtime; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_check_install_runtime_makefile) : $(check_install_runtime_dependencies) build_library_links
	$(echo) "(constituents.make) Building check_install_runtime.make"; \
	  $(cmtexe) -f=$(bin)check_install_runtime.in -tag=$(tags) $(check_install_runtime_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_check_install_runtime_makefile) check_install_runtime
else
$(cmt_local_check_install_runtime_makefile) : $(check_install_runtime_dependencies) $(cmt_build_library_linksstamp) $(bin)check_install_runtime.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_check_install_runtime) ] || \
	  [ ! -f $(cmt_final_setup_check_install_runtime) ] || \
	  $(not_check_install_runtime_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building check_install_runtime.make"; \
	  $(cmtexe) -f=$(bin)check_install_runtime.in -tag=$(tags) $(check_install_runtime_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_check_install_runtime_makefile) check_install_runtime; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(check_install_runtime_extratags) build constituent_makefile -out=$(cmt_local_check_install_runtime_makefile) check_install_runtime

check_install_runtime :: $(check_install_runtime_dependencies) $(cmt_local_check_install_runtime_makefile) dirs check_install_runtimedirs
	$(echo) "(constituents.make) Starting check_install_runtime"
	@if test -f $(cmt_local_check_install_runtime_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_runtime_makefile) check_install_runtime; \
	  fi
#	@$(MAKE) -f $(cmt_local_check_install_runtime_makefile) check_install_runtime
	$(echo) "(constituents.make) check_install_runtime done"

clean :: check_install_runtimeclean ;

check_install_runtimeclean :: $(check_install_runtimeclean_dependencies) ##$(cmt_local_check_install_runtime_makefile)
	$(echo) "(constituents.make) Starting check_install_runtimeclean"
	@-if test -f $(cmt_local_check_install_runtime_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_runtime_makefile) check_install_runtimeclean; \
	fi
	$(echo) "(constituents.make) check_install_runtimeclean done"
#	@-$(MAKE) -f $(cmt_local_check_install_runtime_makefile) check_install_runtimeclean

##	  /bin/rm -f $(cmt_local_check_install_runtime_makefile) $(bin)check_install_runtime_dependencies.make

install :: check_install_runtimeinstall ;

check_install_runtimeinstall :: $(check_install_runtime_dependencies) $(cmt_local_check_install_runtime_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_check_install_runtime_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_runtime_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_check_install_runtime_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : check_install_runtimeuninstall

$(foreach d,$(check_install_runtime_dependencies),$(eval $(d)uninstall_dependencies += check_install_runtimeuninstall))

check_install_runtimeuninstall : $(check_install_runtimeuninstall_dependencies) ##$(cmt_local_check_install_runtime_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_check_install_runtime_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_runtime_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_check_install_runtime_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: check_install_runtimeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ check_install_runtime"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ check_install_runtime done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_check_install_joboptions_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_check_install_joboptions_has_target_tag

cmt_local_tagfile_check_install_joboptions = $(bin)$(InDetPhysValMonitoring_tag)_check_install_joboptions.make
cmt_final_setup_check_install_joboptions = $(bin)setup_check_install_joboptions.make
cmt_local_check_install_joboptions_makefile = $(bin)check_install_joboptions.make

check_install_joboptions_extratags = -tag_add=target_check_install_joboptions

else

cmt_local_tagfile_check_install_joboptions = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_check_install_joboptions = $(bin)setup.make
cmt_local_check_install_joboptions_makefile = $(bin)check_install_joboptions.make

endif

not_check_install_joboptions_dependencies = { n=0; for p in $?; do m=0; for d in $(check_install_joboptions_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
check_install_joboptionsdirs :
	@if test ! -d $(bin)check_install_joboptions; then $(mkdir) -p $(bin)check_install_joboptions; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)check_install_joboptions
else
check_install_joboptionsdirs : ;
endif

ifdef cmt_check_install_joboptions_has_target_tag

ifndef QUICK
$(cmt_local_check_install_joboptions_makefile) : $(check_install_joboptions_dependencies) build_library_links
	$(echo) "(constituents.make) Building check_install_joboptions.make"; \
	  $(cmtexe) -tag=$(tags) $(check_install_joboptions_extratags) build constituent_config -out=$(cmt_local_check_install_joboptions_makefile) check_install_joboptions
else
$(cmt_local_check_install_joboptions_makefile) : $(check_install_joboptions_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_check_install_joboptions) ] || \
	  [ ! -f $(cmt_final_setup_check_install_joboptions) ] || \
	  $(not_check_install_joboptions_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building check_install_joboptions.make"; \
	  $(cmtexe) -tag=$(tags) $(check_install_joboptions_extratags) build constituent_config -out=$(cmt_local_check_install_joboptions_makefile) check_install_joboptions; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_check_install_joboptions_makefile) : $(check_install_joboptions_dependencies) build_library_links
	$(echo) "(constituents.make) Building check_install_joboptions.make"; \
	  $(cmtexe) -f=$(bin)check_install_joboptions.in -tag=$(tags) $(check_install_joboptions_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_check_install_joboptions_makefile) check_install_joboptions
else
$(cmt_local_check_install_joboptions_makefile) : $(check_install_joboptions_dependencies) $(cmt_build_library_linksstamp) $(bin)check_install_joboptions.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_check_install_joboptions) ] || \
	  [ ! -f $(cmt_final_setup_check_install_joboptions) ] || \
	  $(not_check_install_joboptions_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building check_install_joboptions.make"; \
	  $(cmtexe) -f=$(bin)check_install_joboptions.in -tag=$(tags) $(check_install_joboptions_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_check_install_joboptions_makefile) check_install_joboptions; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(check_install_joboptions_extratags) build constituent_makefile -out=$(cmt_local_check_install_joboptions_makefile) check_install_joboptions

check_install_joboptions :: $(check_install_joboptions_dependencies) $(cmt_local_check_install_joboptions_makefile) dirs check_install_joboptionsdirs
	$(echo) "(constituents.make) Starting check_install_joboptions"
	@if test -f $(cmt_local_check_install_joboptions_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_joboptions_makefile) check_install_joboptions; \
	  fi
#	@$(MAKE) -f $(cmt_local_check_install_joboptions_makefile) check_install_joboptions
	$(echo) "(constituents.make) check_install_joboptions done"

clean :: check_install_joboptionsclean ;

check_install_joboptionsclean :: $(check_install_joboptionsclean_dependencies) ##$(cmt_local_check_install_joboptions_makefile)
	$(echo) "(constituents.make) Starting check_install_joboptionsclean"
	@-if test -f $(cmt_local_check_install_joboptions_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_joboptions_makefile) check_install_joboptionsclean; \
	fi
	$(echo) "(constituents.make) check_install_joboptionsclean done"
#	@-$(MAKE) -f $(cmt_local_check_install_joboptions_makefile) check_install_joboptionsclean

##	  /bin/rm -f $(cmt_local_check_install_joboptions_makefile) $(bin)check_install_joboptions_dependencies.make

install :: check_install_joboptionsinstall ;

check_install_joboptionsinstall :: $(check_install_joboptions_dependencies) $(cmt_local_check_install_joboptions_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_check_install_joboptions_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_joboptions_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_check_install_joboptions_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : check_install_joboptionsuninstall

$(foreach d,$(check_install_joboptions_dependencies),$(eval $(d)uninstall_dependencies += check_install_joboptionsuninstall))

check_install_joboptionsuninstall : $(check_install_joboptionsuninstall_dependencies) ##$(cmt_local_check_install_joboptions_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_check_install_joboptions_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_joboptions_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_check_install_joboptions_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: check_install_joboptionsuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ check_install_joboptions"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ check_install_joboptions done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_check_install_python_modules_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_check_install_python_modules_has_target_tag

cmt_local_tagfile_check_install_python_modules = $(bin)$(InDetPhysValMonitoring_tag)_check_install_python_modules.make
cmt_final_setup_check_install_python_modules = $(bin)setup_check_install_python_modules.make
cmt_local_check_install_python_modules_makefile = $(bin)check_install_python_modules.make

check_install_python_modules_extratags = -tag_add=target_check_install_python_modules

else

cmt_local_tagfile_check_install_python_modules = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_check_install_python_modules = $(bin)setup.make
cmt_local_check_install_python_modules_makefile = $(bin)check_install_python_modules.make

endif

not_check_install_python_modules_dependencies = { n=0; for p in $?; do m=0; for d in $(check_install_python_modules_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
check_install_python_modulesdirs :
	@if test ! -d $(bin)check_install_python_modules; then $(mkdir) -p $(bin)check_install_python_modules; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)check_install_python_modules
else
check_install_python_modulesdirs : ;
endif

ifdef cmt_check_install_python_modules_has_target_tag

ifndef QUICK
$(cmt_local_check_install_python_modules_makefile) : $(check_install_python_modules_dependencies) build_library_links
	$(echo) "(constituents.make) Building check_install_python_modules.make"; \
	  $(cmtexe) -tag=$(tags) $(check_install_python_modules_extratags) build constituent_config -out=$(cmt_local_check_install_python_modules_makefile) check_install_python_modules
else
$(cmt_local_check_install_python_modules_makefile) : $(check_install_python_modules_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_check_install_python_modules) ] || \
	  [ ! -f $(cmt_final_setup_check_install_python_modules) ] || \
	  $(not_check_install_python_modules_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building check_install_python_modules.make"; \
	  $(cmtexe) -tag=$(tags) $(check_install_python_modules_extratags) build constituent_config -out=$(cmt_local_check_install_python_modules_makefile) check_install_python_modules; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_check_install_python_modules_makefile) : $(check_install_python_modules_dependencies) build_library_links
	$(echo) "(constituents.make) Building check_install_python_modules.make"; \
	  $(cmtexe) -f=$(bin)check_install_python_modules.in -tag=$(tags) $(check_install_python_modules_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_check_install_python_modules_makefile) check_install_python_modules
else
$(cmt_local_check_install_python_modules_makefile) : $(check_install_python_modules_dependencies) $(cmt_build_library_linksstamp) $(bin)check_install_python_modules.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_check_install_python_modules) ] || \
	  [ ! -f $(cmt_final_setup_check_install_python_modules) ] || \
	  $(not_check_install_python_modules_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building check_install_python_modules.make"; \
	  $(cmtexe) -f=$(bin)check_install_python_modules.in -tag=$(tags) $(check_install_python_modules_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_check_install_python_modules_makefile) check_install_python_modules; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(check_install_python_modules_extratags) build constituent_makefile -out=$(cmt_local_check_install_python_modules_makefile) check_install_python_modules

check_install_python_modules :: $(check_install_python_modules_dependencies) $(cmt_local_check_install_python_modules_makefile) dirs check_install_python_modulesdirs
	$(echo) "(constituents.make) Starting check_install_python_modules"
	@if test -f $(cmt_local_check_install_python_modules_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_python_modules_makefile) check_install_python_modules; \
	  fi
#	@$(MAKE) -f $(cmt_local_check_install_python_modules_makefile) check_install_python_modules
	$(echo) "(constituents.make) check_install_python_modules done"

clean :: check_install_python_modulesclean ;

check_install_python_modulesclean :: $(check_install_python_modulesclean_dependencies) ##$(cmt_local_check_install_python_modules_makefile)
	$(echo) "(constituents.make) Starting check_install_python_modulesclean"
	@-if test -f $(cmt_local_check_install_python_modules_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_python_modules_makefile) check_install_python_modulesclean; \
	fi
	$(echo) "(constituents.make) check_install_python_modulesclean done"
#	@-$(MAKE) -f $(cmt_local_check_install_python_modules_makefile) check_install_python_modulesclean

##	  /bin/rm -f $(cmt_local_check_install_python_modules_makefile) $(bin)check_install_python_modules_dependencies.make

install :: check_install_python_modulesinstall ;

check_install_python_modulesinstall :: $(check_install_python_modules_dependencies) $(cmt_local_check_install_python_modules_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_check_install_python_modules_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_python_modules_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_check_install_python_modules_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : check_install_python_modulesuninstall

$(foreach d,$(check_install_python_modules_dependencies),$(eval $(d)uninstall_dependencies += check_install_python_modulesuninstall))

check_install_python_modulesuninstall : $(check_install_python_modulesuninstall_dependencies) ##$(cmt_local_check_install_python_modules_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_check_install_python_modules_makefile); then \
	  $(MAKE) -f $(cmt_local_check_install_python_modules_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_check_install_python_modules_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: check_install_python_modulesuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ check_install_python_modules"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ check_install_python_modules done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_InDetPhysValMonitoring_NICOS_Fix_debuginfo_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_InDetPhysValMonitoring_NICOS_Fix_debuginfo_has_target_tag

cmt_local_tagfile_InDetPhysValMonitoring_NICOS_Fix_debuginfo = $(bin)$(InDetPhysValMonitoring_tag)_InDetPhysValMonitoring_NICOS_Fix_debuginfo.make
cmt_final_setup_InDetPhysValMonitoring_NICOS_Fix_debuginfo = $(bin)setup_InDetPhysValMonitoring_NICOS_Fix_debuginfo.make
cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile = $(bin)InDetPhysValMonitoring_NICOS_Fix_debuginfo.make

InDetPhysValMonitoring_NICOS_Fix_debuginfo_extratags = -tag_add=target_InDetPhysValMonitoring_NICOS_Fix_debuginfo

else

cmt_local_tagfile_InDetPhysValMonitoring_NICOS_Fix_debuginfo = $(bin)$(InDetPhysValMonitoring_tag).make
cmt_final_setup_InDetPhysValMonitoring_NICOS_Fix_debuginfo = $(bin)setup.make
cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile = $(bin)InDetPhysValMonitoring_NICOS_Fix_debuginfo.make

endif

not_InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies = { n=0; for p in $?; do m=0; for d in $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
InDetPhysValMonitoring_NICOS_Fix_debuginfodirs :
	@if test ! -d $(bin)InDetPhysValMonitoring_NICOS_Fix_debuginfo; then $(mkdir) -p $(bin)InDetPhysValMonitoring_NICOS_Fix_debuginfo; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)InDetPhysValMonitoring_NICOS_Fix_debuginfo
else
InDetPhysValMonitoring_NICOS_Fix_debuginfodirs : ;
endif

ifdef cmt_InDetPhysValMonitoring_NICOS_Fix_debuginfo_has_target_tag

ifndef QUICK
$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) : $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoring_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) InDetPhysValMonitoring_NICOS_Fix_debuginfo
else
$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) : $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoring_NICOS_Fix_debuginfo) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoring_NICOS_Fix_debuginfo) ] || \
	  $(not_InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoring_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_extratags) build constituent_config -out=$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) InDetPhysValMonitoring_NICOS_Fix_debuginfo; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) : $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies) build_library_links
	$(echo) "(constituents.make) Building InDetPhysValMonitoring_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoring_NICOS_Fix_debuginfo.in -tag=$(tags) $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) InDetPhysValMonitoring_NICOS_Fix_debuginfo
else
$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) : $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies) $(cmt_build_library_linksstamp) $(bin)InDetPhysValMonitoring_NICOS_Fix_debuginfo.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_InDetPhysValMonitoring_NICOS_Fix_debuginfo) ] || \
	  [ ! -f $(cmt_final_setup_InDetPhysValMonitoring_NICOS_Fix_debuginfo) ] || \
	  $(not_InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building InDetPhysValMonitoring_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -f=$(bin)InDetPhysValMonitoring_NICOS_Fix_debuginfo.in -tag=$(tags) $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) InDetPhysValMonitoring_NICOS_Fix_debuginfo; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_extratags) build constituent_makefile -out=$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) InDetPhysValMonitoring_NICOS_Fix_debuginfo

InDetPhysValMonitoring_NICOS_Fix_debuginfo :: $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies) $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) dirs InDetPhysValMonitoring_NICOS_Fix_debuginfodirs
	$(echo) "(constituents.make) Starting InDetPhysValMonitoring_NICOS_Fix_debuginfo"
	@if test -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) InDetPhysValMonitoring_NICOS_Fix_debuginfo; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) InDetPhysValMonitoring_NICOS_Fix_debuginfo
	$(echo) "(constituents.make) InDetPhysValMonitoring_NICOS_Fix_debuginfo done"

clean :: InDetPhysValMonitoring_NICOS_Fix_debuginfoclean ;

InDetPhysValMonitoring_NICOS_Fix_debuginfoclean :: $(InDetPhysValMonitoring_NICOS_Fix_debuginfoclean_dependencies) ##$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile)
	$(echo) "(constituents.make) Starting InDetPhysValMonitoring_NICOS_Fix_debuginfoclean"
	@-if test -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) InDetPhysValMonitoring_NICOS_Fix_debuginfoclean; \
	fi
	$(echo) "(constituents.make) InDetPhysValMonitoring_NICOS_Fix_debuginfoclean done"
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) InDetPhysValMonitoring_NICOS_Fix_debuginfoclean

##	  /bin/rm -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) $(bin)InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies.make

install :: InDetPhysValMonitoring_NICOS_Fix_debuginfoinstall ;

InDetPhysValMonitoring_NICOS_Fix_debuginfoinstall :: $(InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies) $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : InDetPhysValMonitoring_NICOS_Fix_debuginfouninstall

$(foreach d,$(InDetPhysValMonitoring_NICOS_Fix_debuginfo_dependencies),$(eval $(d)uninstall_dependencies += InDetPhysValMonitoring_NICOS_Fix_debuginfouninstall))

InDetPhysValMonitoring_NICOS_Fix_debuginfouninstall : $(InDetPhysValMonitoring_NICOS_Fix_debuginfouninstall_dependencies) ##$(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_InDetPhysValMonitoring_NICOS_Fix_debuginfo_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: InDetPhysValMonitoring_NICOS_Fix_debuginfouninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ InDetPhysValMonitoring_NICOS_Fix_debuginfo"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ InDetPhysValMonitoring_NICOS_Fix_debuginfo done"
endif

#-- end of constituent ------
#-- start of constituents_trailer ------

uninstall : remove_library_links ;
clean ::
	$(cleanup_echo) $(cmt_build_library_linksstamp)
	-$(cleanup_silent) \rm -f $(cmt_build_library_linksstamp)
#clean :: remove_library_links

remove_library_links ::
ifndef QUICK
	$(echo) "(constituents.make) Removing library links"; \
	  $(remove_library_links)
else
	$(echo) "(constituents.make) Removing library links"; \
	  $(remove_library_links) -f=$(bin)library_links.in -without_cmt
endif

#-- end of constituents_trailer ------
